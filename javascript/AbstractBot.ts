/// <reference path="./comms"/>
/// <reference path="./race"/>


module bot {

	import IMessageHandler = comms.IMessageHandler;
	import Message = comms.Message;
	import Ping = comms.Ping;
	import Comms = comms.Comms;

	export class AbstractBot implements IMessageHandler {

		constructor(
            public server?:Comms
		){}
		handleMessage(msg:Message):void {
			if (typeof this[msg.msgType] === 'function') {
				this[msg.msgType](msg);
			}
		}

		send(msg:Message):void {
//            console.log("Game tick in msg: " + msg.gameTick);
            try {
                if (isNaN(msg.gameTick)) {
                    msg.gameTick = GAME.tick;
//                    console.log("Adding game tick to message:" + msg.gameTick)
                }
                this.server.send(msg, false);
            } catch (e) {
                console.log("Error sending",e);
            }
		}

		ping():void {
			this.send(new Ping());
		}
	}
}