/// <reference path="./AbstractBot"/>
/// <reference path="./RaceBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>

module bot {

    import Message = comms.Message;
    import CarPosition = race.CarPosition;
    import Piece = track.Piece;

    export class DumbBotNic extends AbstractBot {

        throttle:number = 1;
        oldThrottle:number = 1;

        oldAngle:number = 0;

        pieces:Piece[];
        raceBot:RaceBot;
        lastLaneChoice:string = "Left";

        gameInit(msg:Message):void {
            this.pieces = msg.data.race.track.pieces;

            this.pieces.forEach((piece, i)=> {
                piece['id'] = i;

                if (piece.angle > 5)
                    piece['turn'] = '//////';
                else if (piece.angle < 5)
                    piece['turn'] = '\\\\\\';

                console.log(piece);
            })
        }

        carPositions(rec:Message):void {

            var positions:any[] = rec.data;
            var result:boolean = positions.some(function (pos:CarPosition):boolean {
                if (pos.id.name == "797F") {
                    var angle = Math.abs(pos.angle);
                    var angleBefore = this.oldAngle;
                    var piece = GAME.thisPiece
                    var nextPiece = GAME.getPiece(pos.piecePosition.pieceIndex + 1);
                    var followingPiece = GAME.getPiece(pos.piecePosition.pieceIndex + 2);
                    var idx = pos.piecePosition.pieceIndex;

                    var t = 0.6;
                    var speed = {
                        0: 0.8,
                        1: 0.6,
                        2: 0.5,
                        3: 0.2,
                        4: 0.3,
                        5: 0.3,
                        6: 0.4,

                        14: 0.2,
                        15: 0.3
                    };

                    if (speed[idx])
                        t = speed[idx];
                    if (piece.angle)
                        t = 0.5;
                    else
                        t = 0.75;

                    GAME.throttle = t;

                    /**
                     * Lane switching ...
                     */
                    var nextSwitchId = GAME.findNextSwitchId(pos.piecePosition.pieceIndex);

                    if (GAME.findNextCorner(nextSwitchId).angle > 5) {
                        if (this.lastLaneChoice == "Left") {
                            this.send(new Message('switchLane', "Right"));
                            console.log('\n  SWAPPING TO THE RIGHT');
                            this.lastLaneChoice = "Right";
                        }
                    }
                    if (GAME.findNextCorner(nextSwitchId).angle < -5) {
                        if (this.lastLaneChoice == "Right") {
                            this.send(new Message('switchLane', "Left"));
                            console.log('\n  SWAPPING TO THE LEFT');
                            this.lastLaneChoice = "Left";
                        }
                    }

                    if (GAME.throttle > 1) {
                        GAME.throttle = 1;
                    }
                    if (GAME.throttle < 0.001) {
                        GAME.throttle = 0.001;
                    }

                    this.send(new Message('throttle', GAME.throttle));

                    if (GAME.throttle != this.oldThrottle) {
                        this.oldThrottle = GAME.throttle;
                        console.log("\nnew throttle " + GAME.throttle);
                    }
                    this.oldAngle = angle;
                    return true;
                }
                return false;
            }, this);
            if (result) {
                rec.handled = true;
                rec.replied = true;
            }
        }
    }

}
