/// <reference path="./AbstractBot"/>
/// <reference path="./RaceBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>

module bot {

    import Message = comms.Message;
    import CarPosition = race.CarPosition;

    export class FasterBot extends AbstractBot {

        throttle:number = 1;
        lastThrottle:number = -1;

        raceBot:RaceBot;

        carPositions(rec:Message):void {

            var positions:any[] = rec.data;
            var result:boolean = positions.some(function (pos:CarPosition):boolean {
                if (pos.id.name == "797F") {
                    var lastPiece = this.raceBot.getPiece(pos.piecePosition.pieceIndex - 1);
                    var thisPiece = this.raceBot.getPiece(pos.piecePosition.pieceIndex);
                    var nextPiece = this.raceBot.getPiece(pos.piecePosition.pieceIndex + 1);
                    if (lastPiece.length && thisPiece.length && nextPiece.radius &&
                        pos.piecePosition.inPieceDistance > 0.2) { // into corner after long straight
                        this.throttle = 0.3;
                    } else if (nextPiece.radius &&
                        pos.piecePosition.inPieceDistance > 0.5) { // into corner
                        this.throttle = 0.5;
                    } else if (thisPiece.length) { // into straight
                        this.throttle += 0.1;
                    }

                    if (this.throttle > 1) {
                        this.throttle = 1;
                    }
                    if (this.throttle < 0.001) {
                        this.throttle = 0.001;
                    }

                    if (this.throttle != this.lastThrottle) {
                        this.send(new Message('throttle', this.throttle));
                        this.lastThrottle = this.throttle;
                    }

                    return true;
                }
                return false;
            }, this);
            if (result) {
                rec.handled = true;
                rec.replied = true;
            }
        }
    }

}
