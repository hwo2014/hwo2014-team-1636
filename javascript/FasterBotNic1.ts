/// <reference path="./AbstractBot"/>
/// <reference path="./RaceBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>

module bot {

    import Message = comms.Message;
    import CarPosition = race.CarPosition;

    export class FasterBotNic1 extends AbstractBot {

        throttle:number = 1;
        oldAngle:number = 0;

        raceBot:RaceBot;
        lastLaneChoice:string = "Left";

        carPositions(rec:Message):void {

            var positions:any[] = rec.data;
            var result:boolean = positions.some(function (pos:CarPosition):boolean {
                if (pos.id.name == "797F") {
                    var angle = Math.abs(pos.angle);
                    var angleBefore = this.oldAngle;
                    var nextPiece = GAME.getPiece(pos.piecePosition.pieceIndex + 1);
                    var followingPiece = GAME.getPiece(pos.piecePosition.pieceIndex + 2);
                    if (pos.piecePosition.inPieceDistance > 0.2) {
                        // 0.2     60 75 70 : 8.90
                        // 0.2     60 77 72 : 8.78
                        // 0.2  55 60 79 73 : 8.77 w lane switch
                        // 0.2  52 67 81 72 : 8.77
                        // 0.2  54 68 81 66 : 8.58
                        // 0.2  55 67 81 65 : 8.55 ---------
                        // 0.2  56 68 80 63 : 8.55
                        // 0.2  58 68 75 63 : 8.58
                        // 0.2  58 69 76 65 : 8.55
                        // 0.2  56 69 77 68 : 8.60
                        // 0.2  56 69 77 70 : 8.57
                        // 0.2  58 69 76 66 : 8.52
                        // 0.2 565 69 77 70 : 8.53
                        // 0.2 565 69577 70 : 8.53
                        // 0.2  58 69 76 66 : 8.52

                        if (nextPiece.radius) { // corner coming!
                            if (Math.abs(nextPiece.angle) > 40) {
                                this.throttle = 0.58; // sharp corner, dangerous in 1st corner
                            }
                            else {
                                this.throttle = 0.69; // easy corner, dangerous in 2nd corner
                            }
                        } else {
                            if (angle < angleBefore)
                                this.throttle = 0.76;
                            else
                                this.throttle = 0.665;
                        }

                        /**
                         * Lane switching ...
                         */
                        var nextSwitchId = GAME.findNextSwitchId(pos.piecePosition.pieceIndex);

                        if (GAME.findNextCorner(nextSwitchId).angle > 5) {
                            if (this.lastLaneChoice == "Left") {
                                this.send(new Message('switchLane', "Right"));
                                console.log('\n  SWAPPING TO THE RIGHT');
                                this.lastLaneChoice = "Right";
                            }
                        }
                        if (GAME.findNextCorner(nextSwitchId).angle < -5) {
                            if (this.lastLaneChoice == "Right") {
                                this.send(new Message('switchLane', "Left"));
                                console.log('\n  SWAPPING TO THE LEFT');
                                this.lastLaneChoice = "Left";
                            }
                        }

//                        console.log(this.raceBot.findNextCorner(pos.piecePosition.pieceIndex).angle);
//                        console.log(this.raceBot.findNextCorner(pos.piecePosition.pieceIndex));
                    }

                    if (GAME.throttle > 1) {
                        GAME.throttle = 1;
                    }
                    if (GAME.throttle < 0.001) {
                        GAME.throttle = 0.001;
                    }
                    this.send(new Message('throttle', GAME.throttle));
                    this.oldAngle = angle;
                    return true;
                }
                return false;
            }, this);
            if (result) {
                rec.handled = true;
                rec.replied = true;
            }
        }
    }

}
