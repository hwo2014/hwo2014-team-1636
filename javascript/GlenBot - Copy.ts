/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
/// <reference path="./track"/>
/// <reference path="./game"/>

declare var GAME:game.GameState;

module bot {

    import Piece = track.Piece;
    import Message = comms.Message;
    import CarPosition = race.CarPosition;

    export class GlenBot extends AbstractBot {

        lastAngle:number;
        lastSpeed:number;

        fSlip:number = 0;
        v1:number = 0;
        v2:number = 0;
        v3:number = 0;
        k:number = 0;

        carPositions(rec:Message):void {
            if (rec.handled)
                return;

            if (GAME.crashed) {
                rec.handled = true;
                return;
            }

            var absAngle:number = Math.abs(GAME.thisPosition.angle);
            var nextPiece = GAME.getPiece(GAME.thisPosition.piecePosition.pieceIndex + 1);
            var toNextPiece:number = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;

            GAME.targetCornerSpeed = 100;
            var brakingDistance = 0;

            if (GAME.calibrating) {
                GAME.state = "Calibrating";
                switch(GAME.tick) {
                    case 1:
                        this.v1 = GAME.speed;
                        break;
                    case 2:
                        this.v2 = GAME.speed;
                        this.k = ( this.v1 - ( this.v2 - this.v1) ) / (this.v1 * this.v1) * GAME.throttle;

                        console.log("Car drag constant", this.k);
                        break;
                    case 3:
                        this.v3 = GAME.speed;
                        var th = ( GAME.throttle / this.k );
                        GAME.carMass = 1.0 / ( Math.log( (this.v3 - th ) / ( this.v2 - th ) ) / -this.k );

                        console.log("Car mass is ", GAME.carMass);
                        break;
                }
                if (GAME.thisPosition.angle != 0) {
                    var v = GAME.speed;
                    var r = GAME.thisCornerRadius;
                    var a = GAME.thisPosition.angle;
                    var b = (v/r)/Math.PI*180;
                    var bSlip = b - a;
                    var vThresh = bSlip/360*(2*Math.PI*r);
                    this.fSlip = vThresh*vThresh/r;
                    GAME.calibrating = false;
                }
            }
            // Don't do else here!
            if (!GAME.calibrating) {

                var thisCornerSpeed = Math.sqrt(this.fSlip * GAME.thisCornerRadius);
                var nextCornerSpeed = Math.sqrt(this.fSlip * GAME.nextCornerRadius);
                GAME.targetCornerSpeed = Math.min(thisCornerSpeed, nextCornerSpeed);
                var ticksNeeded = ( Math.log((GAME.targetCornerSpeed - ( 0 / this.k ) ) / (GAME.speed - ( 0 / this.k ) )) * GAME.carMass ) / ( -this.k );
                var roundedTicks = Math.ceil(ticksNeeded);
                var c = ( GAME.throttle/this.k );
                var a = (GAME.speed - c );
                for (var i = 1; i <= roundedTicks; i++) {
                    brakingDistance += a * Math.pow(Math.E, (-this.k * i)/GAME.carMass) + c;
                }
//                console.log("Braking distance:", brakingDistance, ",", GAME.distanceToCorner);
            }
            if (GAME.canSeeFinish) {
                GAME.state = "Sprint to finish line";
                GAME.throttle = 1;
            } else if (brakingDistance > GAME.distanceToCorner
                && GAME.speed > GAME.targetCornerSpeed) {
                GAME.state = "Preemptive braking " + brakingDistance.toFixed(1);
                GAME.throttle = 0;
            } else if (nextPiece.length &&
                toNextPiece < 50 &&
                absAngle < this.lastAngle) { // into straight
                GAME.state = "Throttle up into straight";
                GAME.throttle = 1;
            } else if (GAME.thisPiece.length) {
                GAME.state = "Straight";
                GAME.throttle = 1;
            } else {
//                if (GAME.speed < GAME.targetCornerSpeed) {
//                    GAME.throttle = GAME.targetCornerSpeed * 1.1 * this.k;//(GAME.speed + (GAME.targetCornerSpeed - GAME.speed) * 0.1) * this.k;
//                    GAME.state = "Speed up to target speed";
//                } else {
//                    GAME.throttle = GAME.targetCornerSpeed * 0.9 * this.k;//(GAME.speed - (GAME.speed - GAME.targetCornerSpeed) * 0.1) * this.k;
//                    GAME.state = "Slow to target speed";
//                }
            }

            this.lastSpeed = GAME.speed;

//            if (GAME.calibrating) // educated guess that hopefully can be corrected
//                GAME.throttle = Math.min(GAME.throttle, Math.sqrt(0.0047 * GAME.nextCornerRadius));

            if (GAME.throttle > 1) {
                GAME.throttle = 1;
            }
            if (GAME.throttle < 0) {
                GAME.throttle = 0;
            }
            if (isNaN(GAME.throttle)) {
                GAME.throttle = 0.5;
            }
            if (GAME.speed == 0 && GAME.throttle == 0) {
                GAME.state = "Stalled :)";
                GAME.throttle = 0.5;
            }
            this.lastAngle = absAngle;
            if (GAME.carMass != 0) {
                var ts = 6.5;
                // ticks = ( ln ( (v - ( h/k ) )/(v(0) - ( h/k ) ) ) * m ) / ( -k )
                var ticksNeeded = ( Math.log((ts - ( 1 / this.k ) ) / (GAME.speed - ( 1 / this.k ) )) * GAME.carMass ) / ( -this.k );
                if (ticksNeeded > 1 && Math.abs(GAME.speed - ts) > 0.1)
                    GAME.throttle = 1;
                else
                    GAME.throttle = ts * this.k;
            } else {
                GAME.throttle = 1;
            }
//            console.log("Sending throttle " + GAME.throttle);
            this.send(new Message('throttle', GAME.throttle));
            rec.handled = true;
            rec.replied = true;
        }

        crash(msg:Message):void {

            if (msg.data.color != GAME.carColor)
                return;
            GAME.crashed = true;

            trace(JSON.stringify(msg, null, 2));
            // we can't do this it will quit even on qualifying
            if (TRIAL)
                setTimeout(()=> {
                    process.exit();
                }, 10000);
        }
    }

}
