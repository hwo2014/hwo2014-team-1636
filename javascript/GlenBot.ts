/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
/// <reference path="./track"/>
/// <reference path="./game"/>

declare var GAME:game.GameState;

module bot {

    import Piece = track.Piece;
    import Message = comms.Message;
    import CarPosition = race.CarPosition;

    export class GlenBot extends AbstractBot {

        lastAngle:number;
        lastSpeed:number;

        // Make this bigger to brake later into corners
        brakeForce:number = 0.15;
        // Make this bigger to make all corners faster
        brakeOffset:number = 2.1;
        // Make this bigger to make corners with larger radius faster
        brakeEffect:number = 13.3;
        // Change how the radius affects braking speed/distance
        radiusModifier:number = 1.393582722;
        // make this lower to brake faster when sliding out
        reactiveBrake:number = 20.16;

        crashes:number = 0;
        maxAngle:number = 0;

        fSlip:number = 0;
        v1:number = 0;
        v2:number = 0;
        v3:number = 0;
        k:number = 0;

        // 40 germany   0
        // 70 finland   ?
        // 90 france   0.5+
        // 100 usa     0.5++
        ba:number = -0.0000833333333333332;
        bb:number = 0.0283333333333333;
        bc:number = -1.575;

        slidingBrake:number = 0.7;

        // Make this bigger to accelerate through corners faster
        reactiveThrottle:number = 0.1;
        // Make this bigger to coast through corners faster
        coastThrottle:number = 0.01;

        stage:number = 0;

        gameInit(msg:Message):void {
            console.log("Corner calc");
            console.log("  ", 50, this.getCornerSpeed(50, 5), "should be about 4");
            console.log("  ", 100, this.getCornerSpeed(100, 5), "should be about 6");
            console.log("  ", 200, this.getCornerSpeed(200, 5), "should be about 10");
        }

        getCornerSpeed(radius:number, mass:number):number {
            var speedDivisor = mass * (this.brakeEffect * (GAME.car.dimensions.guideFlagPosition / (GAME.car.dimensions.length / 2)));
            return ((this.radiusModifier * radius) / speedDivisor) + this.brakeOffset;
        }

        calibrate():void {

            if (GAME.calibrating) {
                GAME.state = "Calibrating";
                if (GAME.speed != 0) {
                    switch (this.stage) {
                        case 0:
                            this.v1 = GAME.speed;
                            this.stage++;
                            console.log("V1",this.v1);
                            break;
                        case 1:
                            this.v2 = GAME.speed;
                            console.log("V2",this.v2);
                            this.k = ( this.v1 - ( this.v2 - this.v1) ) / (this.v1 * this.v1) * GAME.throttle;

                            console.log("Car drag constant", this.k ,this.v1, this.v2,GAME.throttle);
                            this.stage++;
                            break;
                        case 2:
                            this.v3 = GAME.speed;
                            var th = ( GAME.throttle / this.k );
                            GAME.carMass = 1.0 / ( Math.log((this.v3 - th ) / ( this.v2 - th )) / -this.k );

                            console.log("Car mass is ", GAME.carMass);
                            this.stage++;
                            break;
                    }
                }
                if (GAME.thisPosition.angle != 0) {
                    var v = GAME.speed;
                    var r = GAME.thisCornerRadius;
                    var a = GAME.thisPosition.angle;
                    var b = (v/r)/Math.PI*180;
                    var bSlip = b - a;
                    var vThresh = bSlip/360*(2*Math.PI*r);
                    this.fSlip = vThresh*vThresh/r;
                    GAME.calibrating = false;
                }
            }
        }

        carPositions(rec:Message):void {

            this.calibrate();

            if (rec.handled)
                return;

            if (GAME.crashed) {
                rec.handled = true;
                return;
            }


            var absAngle:number = Math.abs(GAME.thisPosition.angle);
            this.maxAngle = Math.max(absAngle, this.maxAngle);
//            var lastLastPiece = GAME.getPiece(GAME.thisPosition.piecePosition.pieceIndex - 2);
            var nextPiece = GAME.getPiece(GAME.thisPosition.piecePosition.pieceIndex + 1);
            var toNextPiece:number = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;

            GAME.targetCornerSpeed = 100;
            if (GAME.nextCorner && GAME.carMass) {
                GAME.targetCornerSpeed = this.getCornerSpeed(GAME.nextCornerRadius, GAME.carMass);
//                console.log("Radius: " + GAME.nextCorner.radius.toFixed(0) + " Target: ", targetCornerSpeed.toFixed(2));
            }

            var brakingDistance = 0;
            if (GAME.speed > GAME.targetCornerSpeed) {
                for (var i = GAME.speed; i > GAME.targetCornerSpeed; i -= this.brakeForce) {
                    brakingDistance += i;
                }
            }

//            console.log("Braking distance:", brakingDistance, ",", GAME.distanceToCorner);
            if (GAME.canSeeFinish) {
                GAME.state = "Sprint to finish line";
                GAME.throttle = 1;
            } else if (brakingDistance > GAME.distanceToCorner
                && GAME.speed > GAME.targetCornerSpeed) {
                GAME.state = "Preemptive braking";
                GAME.throttle = 0.00;
            } else if (nextPiece.length &&
                toNextPiece < 50 &&
                absAngle < this.lastAngle) { // into straight
                GAME.state = "Throttle up into straight";
                GAME.throttle = 1;
            } else if (GAME.thisPiece.length) {
                GAME.state = "Straight";
                GAME.throttle = 1;
            } else {

//                if ((GAME.thisPosition.angle < -1 && GAME.thisPiece.angle > 0) ||
//                    (GAME.thisPosition.angle > 1 && GAME.thisPiece.angle < 0)) {
//                    GAME.state = "Brake about to swing out";
//                    GAME.throttle = 0;
//                } else
                // Slow down propotionately to the angle we are on
                if (absAngle > this.lastAngle && absAngle - this.lastAngle > 0.3) {
                    GAME.state = "Brake because we are sliding out";
                    GAME.throttle -= absAngle / this.reactiveBrake;
                    this.slidingBrake = Math.max(0, this.ba * absAngle * absAngle + this.bb * absAngle + this.bc);
                    GAME.throttle = Math.max(GAME.throttle, this.slidingBrake);
                } else {
//                    if (absAngle < this.lastAngle) {
//                        GAME.state = "Increase drift";
//                        GAME.throttle += this.reactiveThrottle;
//                    } else {
//                        GAME.state = "Coast through corner";
//                        // Do nothing?
//                        GAME.throttle += this.coastThrottle;
//                    }
//                    GAME.throttle += 3/absAngle; // speed up faster depending on angle
//                    GAME.throttle = GAME.speed / 10;
//                    if (this.lastSpeed < GAME.speed)
//                        GAME.throttle += 0.1;
//                    else if (this.lastSpeed - GAME.speed > 0.2) {
//                        GAME.throttle -= 0.1;
//                    }
                    GAME.state = "Accelerate through corner";
                    GAME.throttle = 1;
                }
            }

            this.lastSpeed = GAME.speed;

            if (GAME.throttle > 1) {
                GAME.throttle = 1;
            }
            if (GAME.throttle < 0) {
                GAME.throttle = 0;
            }
            if (isNaN(GAME.throttle)) {
                GAME.throttle = 0;
            }
            if (GAME.speed == 0 && GAME.throttle == 0) {
                GAME.state = "Stalled :)";
                GAME.throttle = 1;
            }
            this.lastAngle = absAngle;
//            console.log("Sending throttle " + GAME.throttle);
            this.send(new Message('throttle', GAME.throttle));
            rec.handled = true;
            rec.replied = true;
        }

        crash(msg:Message):void {
            if (msg.data.color != GAME.carColor)
                return;
            this.crashes++;
            this.brakeOffset -= 0.05;
            trace("Crashed " + this.crashes + " this lap so go slower :(");
        }

        lapFinished(msg:Message):void {
            if (msg.data.car.color != GAME.carColor)
                return;
            if (this.crashes == 0 && this.maxAngle < 58) {
                this.brakeOffset += 0.025;
                trace("No crashes, try to go faster!");
            }
            this.crashes = 0;
        }
    }

}
