var fs = require('fs');
var exec = require('child_process').exec;

var hammer;
(function (hammer) {
    var Time = (function () {
        function Time() {
            var _this = this;
            this.cmd = "cd .. & trial testserver.helloworldopen.com 8091 797F i237nvaJ2cPdOQ";
            this.originalArgs = [30, 300, 0.15, 2.3, 11.8, 20];
            this.deviation = 0.59;
            this.trackname = '';
            this.jobPool = [true, true, true, true, true];
            this.jobColor = [Color.CYANB, Color.GREENB, Color.PINKB, Color.YELLOWB, Color.PINK, Color.REDB, Color.RED];
            if (!process.argv[2]) {
                console.log("requires track name [ keimola | germany | usa | france ]");
                return;
            }

            this.trackname = process.argv[2];

            switch (this.trackname) {
                case 'germany':
                    this.originalArgs = [37.21, 242.657, 0.248, 2.357, 11.877, 19.573];
                    break;
                case 'keimola':
                    this.originalArgs = [38.977, 298.07, 0.266, 3.416, 11.358, 24.031];
                    break;
                case 'usa':
                    this.originalArgs = [26, 275, 0.548, 2.802, 12.128, 18.168];
                    this.originalArgs = [25, 255, 0.3, 2.3, 11.5, 20];
                    break;
                case 'france':
                    this.originalArgs = [35, 242.657, 0.4, 2.357, 11.877, 19.573];
                    break;
                default:
                    console.log("requires track name [ keimola | germany | usa | france ]");
                    return;
            }

            setInterval(function () {
                var done = false;

                _this.jobPool.forEach(function (free, index) {
                    if (!done && free == true) {
                        _this.jobPool[index] = false;
                        console.log(Color.WHITEB + "\n-- Starting job " + index + Color.RESET);

                        var deviatedArgs = [];
                        _this.originalArgs.forEach(function (ratio, i) {
                            var dev = ratio * _this.deviation;
                            deviatedArgs[i] = (ratio + (-dev + (Math.random() * dev * 2))).toFixed(6);
                        });

                        _this.initPool(index, deviatedArgs, function () {
                            _this.jobPool[index] = true;
                        });
                        done = true;
                    }
                });
            }, 5200);
        }
        Time.prototype.initPool = function (job, args, callback) {
            var _this = this;
            console.log("New Job " + args.join(' '));
            var command = this.cmd + " " + this.trackname + " 1 " + args.join(' ') + "\n\n";

            this.trialrun(this.cmd + " " + this.trackname + " 1 " + args.join(' '), function (progress) {
                process.stdout.write(_this.jobColor[job] + progress + Color.RESET);
            }, function (stdout) {
                var fileName = "run-" + Math.random() + ".txt";
                fs.writeFile(fileName, command + stdout, function (err) {
                    console.log(err ? err : "job done");
                    callback();
                });
            });
        };

        Time.prototype.trialrun = function (command, progressCallback, finishedCallback) {
            var proc = exec(command, function (error, stdout, stderr) {
                if (error !== null) {
                    console.log('ERROR : ' + error);
                }
            }), stdout = [];
            proc.stdout.setEncoding('utf8');
            proc.stdout.setTimeout(10 * 60 * 1000);

            proc.stdout.on('data', function (chunk) {
                stdout.push(chunk);
                progressCallback(chunk);
            });

            proc.stdout.on('end', function () {
                finishedCallback(stdout.join(''));
            });
        };
        return Time;
    })();
    hammer.Time = Time;

    var Color = (function () {
        function Color() {
        }
        Color.RESET = "\x1b[0m";
        Color.RED = "\x1b[31m";
        Color.REDB = "\x1b[1;31m";
        Color.GREEN = "\x1b[32m";
        Color.GREENB = "\x1b[1;32m";
        Color.YELLOW = "\x1b[33m";
        Color.YELLOWB = "\x1b[1;33m";
        Color.CYAN = "\x1b[36m";
        Color.CYANB = "\x1b[1;36m";
        Color.PINK = "\x1b[35m";
        Color.PINKB = "\x1b[1;35m";
        Color.WHITEB = "\x1b[1;37m";
        return Color;
    })();
    hammer.Color = Color;
})(hammer || (hammer = {}));


var STOP = new hammer.Time();
//# sourceMappingURL=HammerTime.js.map
