declare var require;
declare var process;
var fs = require('fs');
var exec = require('child_process').exec;

module hammer {
    export class Time {

        /**
         * keimola
         * germany
         * usa
         */
        cmd = "cd .. & trial testserver.helloworldopen.com 8091 797F i237nvaJ2cPdOQ";
        // turbo before corner, turbo require distance
        // brake force, brake offset, brake effect
        originalArgs = [30, 300, 0.15, 2.3, 11.8, 20]; // Overwritten below!
        deviation = 0.59;
        trackname = '';
        jobPool = [true, true, true, true, true]; // availability by thread
        jobColor = [Color.CYANB, Color.GREENB, Color.PINKB, Color.YELLOWB, Color.PINK, Color.REDB, Color.RED]; // availability by thread

        constructor() {

            if (!process.argv[2]) {
                console.log("requires track name [ keimola | germany | usa | france ]");
                return;
            }

            this.trackname = process.argv[2];

            switch (this.trackname) {
                case 'germany':
                    this.originalArgs = [37.21, 242.657, 0.248, 2.357, 11.877, 19.573]; // Germany
                    break;
                case 'keimola':
                    this.originalArgs = [38.977, 298.07, 0.266, 3.416, 11.358, 24.031]; // Finland
                    break;
                case 'usa':
                    this.originalArgs = [26, 275, 0.548, 2.802, 12.128, 18.168]; // USA 5.05
                    this.originalArgs = [25, 255, 0.3, 2.3, 11.5, 20]; // USA 5.05
                    break;
                case 'france':
                    this.originalArgs = [35, 242.657, 0.4, 2.357, 11.877, 19.573];
                    break;
                default:
                    console.log("requires track name [ keimola | germany | usa | france ]");
                    return;
            }

            setInterval(()=> {
                var done = false;
//                console.log(this.jobPool);
                this.jobPool.forEach((free, index)=> {
                    if (!done && free == true) {
                        this.jobPool[index] = false;
                        console.log(Color.WHITEB + "\n-- Starting job " + index + Color.RESET);

                        var deviatedArgs = [];
                        this.originalArgs.forEach((ratio, i)=> {
//                            if (i == 2) {
                                var dev = ratio * this.deviation;
                                deviatedArgs[i] = (ratio + (-dev + (Math.random() * dev * 2))).toFixed(6);
//                              deviatedArgs[i] = (ratio + (-this.deviation + Math.random() * (this.deviation * 2))).toFixed(3);
//                            }
                        });

                        this.initPool(index, deviatedArgs, ()=> {
                            this.jobPool[index] = true;
                        });
                        done = true;
                    }
                });
            }, 5200);
        }

        initPool(job:number, args:number[], callback:()=>void) {

            console.log("New Job " + args.join(' '));
            var command = this.cmd + " " + this.trackname + " 1 " + args.join(' ') + "\n\n";
//            this.trialrun("cd .. & trial testserver.helloworldopen.com 8091 797F i237nvaJ2cPdOQ keimola 1 30 300 0.09 2.3 11.8 20",
            this.trialrun(this.cmd + " " + this.trackname + " 1 " + args.join(' '),
                (progress)=> {
                    process.stdout.write(this.jobColor[job] + progress + Color.RESET);
                }, (stdout) => {
                    var fileName = "run-" + Math.random() + ".txt";
                    fs.writeFile(fileName, command + stdout, function (err) {
                        console.log(err ? err : "job done");
                        callback();
                    });
                }
            )
        }

        trialrun(command, progressCallback, finishedCallback) {
            var proc = exec(command, (error, stdout, stderr)=> {
//                    console.log('stdout: ' + stdout);
//                    console.log('stderr: ' + stderr);
                    if (error !== null) {
                        console.log('ERROR : ' + error);
                    }
                }),
                stdout = [];
            proc.stdout.setEncoding('utf8');
            proc.stdout.setTimeout(10 * 60 * 1000);

            proc.stdout.on('data', function (chunk) {
                stdout.push(chunk);
                progressCallback(chunk);
//                console.log(chunk);
            });

            proc.stdout.on('end', function () {
                finishedCallback(stdout.join(''));
            });
        }


    }

    export class Color {
        public static RESET = "\x1b[0m";
        public static RED = "\x1b[31m";
        public static REDB = "\x1b[1;31m";
        public static GREEN = "\x1b[32m";
        public static GREENB = "\x1b[1;32m";
        public static YELLOW = "\x1b[33m";
        public static YELLOWB = "\x1b[1;33m";
        public static CYAN = "\x1b[36m";
        public static CYANB = "\x1b[1;36m";
        public static PINK = "\x1b[35m";
        public static PINKB = "\x1b[1;35m";
        public static WHITEB = "\x1b[1;37m";
    }
}

// shut up intellij
interface proc {
    stdout:stdout
}
interface stdout {
    on:any;
    write:any;
    setEncoding:any;
}
interface fs { writeFile:any;
}

// duuh duud duuhud
var STOP = new hammer.Time(); //woooooo
