/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
/// <reference path="./track"/>

module bot {

    var fs:any = require("fs");

    import Piece = track.Piece;
    import CarPosition = race.CarPosition;
    import Message = comms.Message;

    export class LoggingBot extends AbstractBot {

        //LOG:any;

        join(rec:Message):void {
            console.log('Joined');
        }

        yourCar(msg:Message):void {
            //this.LOG = fs.createWriteStream("./log" + GAME.carColor + ".csv", {flags: 'w', encoding: 'UTF-8'});
        }

        tournamentEnd(rec:Message):void {
            // this.LOG.close();
        }

        crash(msg:Message):void {
            console.log("Crash:", msg.data);
            if (msg.data.color != GAME.carColor)
                return;
            console.log(GAME.state);
        }

        spawn(msg:Message):void {
            if (msg.data.color != GAME.carColor)
                return;
            console.log(GAME.state);
        }

        gameInit(msg:Message):void {
//            console.log("Track: ", GAME.race.track.pieces);
        }


        carPositions(rec:Message):void {
            if (!GAME.started || GAME.crashed)
                return;

            var toNextPiece:number = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;
//            this.log([
//                GAME.thisPosition.piecePosition.lap,
//                GAME.thisPosition.piecePosition.pieceIndex,
//                GAME.throttle.toFixed(0),
//                GAME.thisPosition.angle.toFixed(1),
//                GAME.speed.toFixed(2),
//                GAME.targetCornerSpeed.toFixed(2),
//                GAME.thisPiece.radius ? GAME.thisPiece.radius.toFixed(0) : '',
//                GAME.thisPiece.angle ? GAME.thisPiece.angle.toFixed(1) : '',
//                GAME.thisPiece.angle ? Math.abs(GAME.thisPiece.angle).toFixed(1) : '',
//                GAME.distanceToCorner.toFixed(1),
//                GAME.state,
//                GAME.turboOn ? "Turbo" : (GAME.turbo ? "T" : ""),
//                    GAME.fastestLap == -1 ? "" : GAME.fastestLap.toFixed(0)
//            ].join(", "));

            console.log([
                GAME.thisPosition.piecePosition.lap,
                GAME.thisPosition.piecePosition.pieceIndex,
                (GAME.throttle * 100).toFixed(5),
                GAME.thisPosition.angle.toFixed(4),
                GAME.speed.toFixed(5),
//                GAME.speedSec.toFixed(2),
//                GAME.acceleration.toFixed(4),
                GAME.targetCornerSpeed.toFixed(1),
                GAME.thisCornerRadius.toFixed(0),
                GAME.thisPiece.angle ? (GAME.thisPiece.radius.toFixed(0) + " " + GAME.thisPiece.angle.toFixed(0) + '° ' + ((GAME.thisPiece.angle < 0) ? "Left Turn" : "Right Turn")) : "Straight",
                GAME.distanceToCorner.toFixed(1),
                GAME.state,
                GAME.turboOn ? "Turbo" : (GAME.turbo ? "T" : ""),
                    GAME.fastestLap == -1 ? "" : GAME.fastestLap.toFixed(0)
            ].join(", "));
        }

        gameStart(rec:Message):void {
            console.log('Race started');
            console.log('Log format');
//            var format:string = 'Lap,Piece,Throttle,CarAngle,Speed,TargetSpeed,PieceRadius,PieceAngle,PieceAbsAngle,PieceType,ToNextCorner,ThrottleState,Turbo,FastestLap';
//            this.log(format);
            var format:string = 'Lap,Piece,Throttle,CarAngle,Speed,TargetSpeed,CornerRadius,PieceType,ToNextCorner,ThrottleState,Turbo,FastestLap';
            console.log(format);
            GAME.started = true;
            rec.handled = true;
        }

        gameEnd(msg:Message):void {
            console.log('Race ended');
            console.log('Results\n', msg.data.results);
            console.log('Best Laps\n', msg.data.bestLaps);
        }

        log(msg:string):void {
//            console.log(msg);
//            this.LOG.write(msg);
//            this.LOG.write("\n");
        }
    }

}
