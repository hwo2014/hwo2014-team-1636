/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>

module bot {

    import Message = comms.Message;

    export class NicBot1 extends AbstractBot {

        velocity:number = 0.7;
        straightline:boolean = false;
        start:number;
        tick:number = 1;
        crashed:boolean = false;

        join(rec:Message):void {
            trace('Joined');
            this.ping();
        }

        gameStart(rec:Message):void {
            trace('---------------------------------- Race started');
            trace('sending throttle');
            this.send(new Message('throttle', 1));

            this.start = new Date().getTime();
        }

        carPositions(rec:Message):void {
//            trace(JSON.stringify(rec, null, 2));

            if (this.crashed)
                return;

            var percent = rec.data[0].piecePosition.inPieceDistance;
            var angle = rec.data[0].angle;

            trace('---------------------------------------------------------------------- ' + this.tick++ + ' / 2000 (approx)');

            trace("lap \t" + rec.data[0].piecePosition.lap + "\t " + rec.data[0].piecePosition.pieceIndex + " / 39 \t angle \t " + angle);

            if (percent > 87)
                this.velocity -= 0.45;
            else
                this.velocity += 0.25;

            if (angle > 3 || angle < -3) {
                if (this.straightline) { // EASE OFF!!
                    this.velocity = 0.1;
                    this.straightline = false;
                } else { // ENTERED CORNER
                    this.velocity += 0.1;
                    if (this.velocity > 0.6) {
                        this.velocity = 0.6;
                    }
                }
            }

            if (angle < 3 || angle > -3) {
                if (!this.straightline) { // GAS IT!!
                    this.straightline = true;
                }
            }

            if (this.velocity > .75)
                this.velocity = .75;

            if (this.velocity < .25)
                this.velocity = .25;

            trace("VELOCITY \t" + this.velocity);

            this.send(new Message('throttle', this.velocity));
        }

        gameEnd(rec:Message):void {
            trace('----------------------------------- Race END');
            this.ping();

            var end = new Date().getTime();
            var time = end - this.start;
        }

        tournamentEnd(rec:Message):void {
            this.gameEnd(rec);
        }

        crash(rec:Message):void {
            trace(JSON.stringify(rec, null, 2));
            this.crashed = true;

            if (rec.data.name == "797F")
                setTimeout(()=> {
                    process.exit();
                }, 10000);

            this.gameEnd(rec);
        }
    }
}

/**{
  "msgType": "carPositions",
  "data": [
    {
      "id": {
        "name": "797F",
        "color": "red"
      },
      "angle": 0.045511780611135096,
      "piecePosition": {
        "pieceIndex": 39,
        "inPieceDistance": 84.63766432298257,
        "lane": {
          "startLaneIndex": 0,
          "endLaneIndex": 0
        },
        "lap": 2
      }
    }
  ]
}**/
