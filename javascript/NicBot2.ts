/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>

module bot {

    import Message = comms.Message;

    export class NicBot2 extends AbstractBot {

        velocity:number = 0.7;
        straightline:boolean = false;
        start:number;
        tick:number = 1;
        crashed:boolean = false;

        join(rec:Message):void {
            trace('Joined');
            this.ping();
        }

        gameStart(rec:Message):void {
            trace('---------------------------------- Race started');
            trace('sending throttle');
            this.send(new Message('throttle', 1));

            this.start = new Date().getTime();
        }

        carPositions(rec:Message):void {
//            trace(JSON.stringify(rec, null, 2));

            if (this.crashed)
                return;

            var percent = rec.data[0].piecePosition.inPieceDistance;
            var angle = Math.round(Number(rec.data[0].angle));

            trace('----------------------------------------------- ' + this.tick++ + ' / 2000 (approx)');

            trace("lap \t" + rec.data[0].piecePosition.lap + "\t " + rec.data[0].piecePosition.pieceIndex + " / 39 \t angle \t " + angle);

            /**
             * This gave 9:17
             */

            if (angle > 5 || angle < -5)
                this.velocity = 0.60;
            else if (angle > 4 || angle < -4)
                this.velocity = 0.625;
            else
                this.velocity = 0.65;

            if(angle == 0)
                this.velocity = 0.7;

            trace("VELOCITY \t" + this.velocity);

            this.send(new Message('throttle', this.velocity));
        }

        gameEnd(rec:Message):void {
            var end = new Date().getTime();
            var time = end - this.start;
            trace('----------------------------------- Race END');
            trace('Took ' + (time / 1000) + " seconds");
            this.ping();
        }

        tournamentEnd(rec:Message):void {
            this.gameEnd(rec);
        }

        crash(rec:Message):void {
            trace(JSON.stringify(rec, null, 2));
            this.crashed = true;

            if (rec.data.name == "797F")
                setTimeout(()=> {
                    process.exit();
                }, 10000);

            this.gameEnd(rec);
        }
    }
}

/**{
  "msgType": "carPositions",
  "data": [
    {
      "id": {
        "name": "797F",
        "color": "red"
      },
      "angle": 0.045511780611135096,
      "piecePosition": {
        "pieceIndex": 39,
        "inPieceDistance": 84.63766432298257,
        "lane": {
          "startLaneIndex": 0,
          "endLaneIndex": 0
        },
        "lap": 2
      }
    }
  ]
}**/
