/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./game"/>
/// <reference path="./race"/>

module bot {
    import Car = car.Car;
    import CarPosition = race.CarPosition;
    import OpponentPosition = race.OpponentPosition;
    import Message = comms.Message;

    export class OpponentBot extends AbstractBot {

        gameStart(msg:Message):void {
            GAME.opponents = new Object();
        }

        carPositions(msg:Message):void {
//            try {
                if (GAME.opponents == null) {
                    GAME.opponents = new Object();
                }
                var positions:CarPosition[] = msg.data;
//                console.log("Positions: " + positions);
                positions.forEach(function (pos:CarPosition):void {
                    if (pos.id.color != GAME.carColor) {
                        this.updateOpponent(pos);
                    }
                }, this);

                this.updateIndexes();
//            } catch (e) {
//                console.log("Error",e);
//            }
        }

        lapFinished(msg:Message):void {
            if (msg.data.car.color == GAME.carColor)
                return;
            console.log("lap finished",msg);
            var opponent:OpponentPosition = GAME.opponents[msg.data.car.color];
            if (opponent && opponent.bestLap > msg.data.lapTime.millis)
                opponent.bestLap = msg.data.lapTime.millis;
        }

        updateOpponent(pos:CarPosition):void {
            var opponent:OpponentPosition = GAME.opponents[pos.id.color];
            if (!opponent) {
                opponent = new OpponentPosition();
            }
            opponent.lane = pos.piecePosition.lane.endLaneIndex;
            opponent.distance = GAME.getDelta(pos, GAME.thisPosition);
            var speed:number = GAME.getDelta(pos, opponent.lastPosition);
            if (Math.abs(opponent.speed - speed) < 1)
                opponent.speed = speed;
            opponent.lastPosition = pos;
            GAME.opponents[pos.id.color] = opponent;
        }

        updateIndexes():void {
            var inFront:OpponentPosition[] = [];
            var inFrontLane:OpponentPosition[] = [];
            var behind:OpponentPosition[] = [];
            var behindLane:OpponentPosition[] = [];
            var thisLane:number = GAME.thisPosition.piecePosition.lane.endLaneIndex;
            for (var color in GAME.opponents) {
                var opponent:OpponentPosition = GAME.opponents[color];
                if (opponent.distance > 0) {
                    inFront.push(opponent);
                    if (opponent.lane == thisLane) {
                        inFrontLane.push(opponent);
                    }
                } else {
                    behind.push(opponent);
                    if (opponent.lane == thisLane) {
                        behindLane.push(opponent);
                    }
                }
            }
            inFront.sort(OpponentPosition.distanceSort);
            inFrontLane.sort(OpponentPosition.distanceSort);
            behind.sort(OpponentPosition.distanceSort);
            behindLane.sort(OpponentPosition.distanceSort);

            GAME.inFront = inFront;
            GAME.inFrontLane = inFrontLane;
            GAME.behind = behind;
            GAME.behindLane = behindLane;
        }

    }
}