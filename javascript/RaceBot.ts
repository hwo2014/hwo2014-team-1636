/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./game"/>
/// <reference path="./race"/>

module bot {
    import Car = car.Car;
    import CarPosition = race.CarPosition;
    import Message = comms.Message;

    export class RaceBot extends AbstractBot {

        lastPos:number = 0;
        forceFound:boolean = false;

        yourCar(msg:Message):void {
            GAME.carColor = msg.data.color;
            console.log("Our car", GAME.carColor);
            msg.handled = true;
        }

        gameInit(msg:Message):void {
            GAME.race = msg.data.race;
            GAME.canSeeFinish = false;
            GAME.lastLap = false;
            var result:boolean = GAME.race.cars.some(function(car:Car):boolean{
                if (car.id.color == GAME.carColor) {
                    GAME.car = car;
//                    GAME.carMass = car.dimensions.length * car.dimensions.width;
                    console.log("car config", GAME.car.dimensions);
                    return true;
                }
                return false;
            }, this);

            this.findLongestStraightPiece();
            this.findAverageCorner();
            msg.handled = result;
        }

        turboAvailable(msg:Message):void {
            // No turbo for you
            if (GAME.crashed)
                return
            GAME.turbo = msg.data;
            msg.handled = true;
        }

        turboStart(msg:Message):void {
            msg.handled = true;
            if (msg.data.color != GAME.carColor)
                return;
            GAME.turbo = null;
            GAME.turboOn = true;
        }

        turboEnd(msg:Message):void {
            msg.handled = true;
            if (msg.data.color != GAME.carColor)
                return;
            GAME.turbo = null;
            GAME.turboOn = false;

        }

        gameStart(msg:Message):void {
//            console.log(msg);
            if (msg.gameTick != null) {
                GAME.lastTick = GAME.tick;
                GAME.tick = msg.gameTick;
            }
        }

        carPositions(msg:Message):void {
//            console.log(msg);
            if (msg.gameTick != null) {
                GAME.lastTick = GAME.tick;
                GAME.tick = msg.gameTick;
            }
//            console.log("Found tick: ", msg.gameTick,GAME.tick);
            if (GAME.crashed)
                return;
            var positions:CarPosition[] = msg.data;
            positions.some(function (pos:CarPosition):boolean {
                if (pos.id.color == GAME.carColor) {
                    if (GAME.thisPosition == null || pos.piecePosition.pieceIndex != GAME.thisPosition.piecePosition.pieceIndex) {
                        GAME.lastPiece = GAME.thisPiece;
                        //GAME.lastPiecePosition = GAME.thisPosition;
                        GAME.lastLength = GAME.thisLength;
                        GAME.thisPiece = GAME.getPiece(pos.piecePosition.pieceIndex);
                        if (GAME.lastPiece == null) {
                            GAME.lastPiece = GAME.getPiece(pos.piecePosition.pieceIndex - 1);
                        }
                    }
                    GAME.thisPosition = pos;
                    GAME.thisLength = GAME.getLength(GAME.thisPiece);
                    GAME.distanceToCorner = GAME.getDistanceToNextCornerPiece();
                    GAME.nextCornerRadius = this.getNextCornerRadius();
                    GAME.thisCornerRadius = this.getCornerRadius();
                    if (GAME.lastLap) {
//                        console.log("Last lap: ", GAME.race.track.pieces.indexOf(GAME.thisPiece), GAME.race.track.pieces.indexOf(GAME.nextCorner));
                        GAME.canSeeFinish = GAME.race.track.pieces.indexOf(GAME.thisPiece) > GAME.race.track.pieces.indexOf(GAME.nextCorner);
                    }

//                    if (!this.forceFound) {
//                        if (GAME.speed > 0) {
//                            GAME.engineForce = GAME.throttle;
//                            var mass:number = GAME.engineForce / GAME.speed;
//                            if (mass > GAME.carMass) {
//                                GAME.carMass = mass;
//                                console.log("Car mass is ", GAME.carMass);
//                            }
//                        }
//                    }

                    var delta:number = (pos.piecePosition.inPieceDistance - this.lastPos);
                    if (delta < 0) {
                        if (this.lastPos < GAME.lastLength) {
                            delta = pos.piecePosition.inPieceDistance + GAME.lastLength - this.lastPos;
                        } else {
                            // don't know how far the last piece really was...
                            console.log("Unknown piece length");
                            delta = pos.piecePosition.inPieceDistance;
                        }
                    }
                    GAME.lastSpeed = GAME.speed;
                    GAME.speed = delta / (GAME.tick - GAME.lastTick);
                    GAME.acceleration = (GAME.speed - GAME.lastSpeed) / (GAME.tick - GAME.lastTick);
                    var duration:number = ((GAME.tick - GAME.lastTick) / 60);
                    GAME.lastSpeedSec = GAME.speedSec;
                    GAME.speedSec = delta / duration / 100; // convert to meters
                    GAME.accelerationSec = (GAME.speedSec - GAME.lastSpeedSec) / duration;


                    this.lastPos = pos.piecePosition.inPieceDistance;

                    return true;
                }
                return false;
            }, this);
//            console.log("END RACEBOT");
        }

        lapFinished(msg:Message):void {
            if (msg.data.car.color != GAME.carColor)
                return;

            if (GAME.fastestLap == -1 || GAME.fastestLap > msg.data.lapTime.millis) {
                GAME.fastestLap = msg.data.lapTime.millis;
            }
//            console.log("LAST LAP? ", msg.data.raceTime.laps, GAME.race.raceSession.laps);
            GAME.lastLap = msg.data.raceTime.laps == GAME.race.raceSession.laps - 1;
        }

        crash(msg:Message):void {
            if (msg.data.color != GAME.carColor)
                return;
            GAME.crashed = true;
            GAME.state = "Crashed";
            msg.handled = true;
        }

        spawn(msg:Message):void {
            if (msg.data.color != GAME.carColor)
                return;

            GAME.throttle = 0.1;
            GAME.turbo = null;
            GAME.turboOn = false;
            GAME.crashed = false;
            GAME.state = "Respawn";
            msg.handled = true;
        }

        getNextCornerRadius():number {
            if (GAME.nextCorner.angle < 0) {
                // left turn
                return GAME.nextCorner.radius + GAME.getLaneOffset(GAME.thisPosition.piecePosition.lane.endLaneIndex);
            } else {
                // right turn
                return GAME.nextCorner.radius - GAME.getLaneOffset(GAME.thisPosition.piecePosition.lane.endLaneIndex);
            }
        }

        getCornerRadius():number {
            if (GAME.thisPiece.length) {
                return 1000;
            } else {
                if (GAME.thisPiece.angle < 0) {
                    // left turn
                    return GAME.thisPiece.radius + GAME.getLaneOffset(GAME.thisPosition.piecePosition.lane.endLaneIndex);
                } else {
                    // right turn
                    return GAME.thisPiece.radius - GAME.getLaneOffset(GAME.thisPosition.piecePosition.lane.endLaneIndex);
                }
            }
        }

        findAverageCorner():void {
            var sumAngle:number = 0;
            var sumRadius:number = 0;
            var pieces:track.Piece[] = GAME.race.track.pieces;
            pieces.forEach((piece)=>{
                if (piece.angle) {
                    sumAngle += Math.abs(piece.angle);
                    sumRadius += piece.radius;
                }
            });
            console.log("Avg corners",(sumAngle/pieces.length).toFixed(2),(sumRadius/pieces.length).toFixed(2));
        }

        findLongestStraightPiece():void {
            var result = 0;
            var bestLength:number = 0;
            var bestIndexes:number[] = [];
            var length:number = 0;
            var lastIndex:number = 0;
            var pieces:track.Piece[] = GAME.race.track.pieces.concat();
            var offset:number = 0;
            while (pieces[0].length) {
                pieces.unshift(pieces.pop());
//                pieces.push(pieces.shift());
                offset++;
                if (offset >= pieces.length) {
                    break;
                }
            }
//            console.log("Piece offset", offset);
            pieces.forEach((piece, index)=>{
//               console.log("Piece",index,piece.length);
               if (piece.length) {
                   length += piece.length;
               } else {
                   if (Math.abs(length - bestLength) < 20) {
                       bestIndexes.push(lastIndex);
                       bestLength = Math.max(length, bestLength);
                   } else if (length > bestLength) {
                       bestLength = length;
                       bestIndexes.length = 0;
                       bestIndexes.push(lastIndex);
                   }
                   length = 0;
                   lastIndex = index + 1;
               }
            });
            if (Math.abs(length - bestLength) < 20) {
                bestIndexes.push(lastIndex);
                bestLength = Math.max(length, bestLength);
            } else if (length > bestLength) {
                bestLength = length;
                bestIndexes.length = 0;
                bestIndexes.push(lastIndex);
            }
            bestIndexes = bestIndexes.map((index):number=>{
                return (index - offset + pieces.length) % pieces.length;
            });
            console.log("Longest straight starts at indexes: " + bestIndexes.join(',') + " and is " + bestLength + "m long");
            GAME.longestStraightPieceIndexes = bestIndexes;
            GAME.longestStraightPiece = bestLength;
        }

    }

}
