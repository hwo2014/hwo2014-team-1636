/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>

module bot {

	import Message = comms.Message;

	export class SimpleBot extends AbstractBot {

		carPositions(rec:Message):void {
			this.send(new Message('throttle', 0.6));
            rec.handled = true;
            rec.replied = true;
		}
	}

}