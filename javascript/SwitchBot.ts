/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
/// <reference path="./track"/>

module bot {

    import Piece = track.Piece;
	import Message = comms.Message;
    import CarPosition = race.CarPosition;

	export class SwitchBot extends AbstractBot {

        lastIndex:number = -2;

		carPositions(rec:Message):void {
            if (GAME.crashed)
                return;
            if (GAME.nextCorner === null)
                return;
            if (GAME.thisPosition.piecePosition.pieceIndex != this.lastIndex) {
                var nextPiece = GAME.getPiece(GAME.thisPosition.piecePosition.pieceIndex + 1);
                var toNextPiece:number = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;
                if (nextPiece.switch && toNextPiece < 30) {
                    var angle:number = 0;
                    var index = GAME.thisPosition.piecePosition.pieceIndex + 2;
                    var toNextSwitch:number = 0;
                    while(true) {
                        var piece = GAME.getPiece(index);
                        if (piece.switch) {
                            break;
                        }
                        if (piece.angle) {
                            angle += piece.angle;
                        }
                        toNextSwitch += GAME.getLength(piece,GAME.thisPosition);
                        index++;
                    }
                    var direction:String;
                    if (angle < -5)
                        direction = "Left";
                    else if (angle > 5)
                        direction = "Right";
                    var newIndex = GAME.thisPosition.piecePosition.lane.startLaneIndex;
                    var offset = GAME.getLaneOffset(newIndex);
                    if (direction != null) {
                        GAME.race.track.lanes.some((lane):boolean=> {
                            var laneOffset = lane.distanceFromCenter;
                            if (direction == "Left" &&
                                laneOffset < offset) {
                                newIndex = lane.index;
                                offset = laneOffset;
                                left = true;
                                return true;
                            } else if (direction == "Right" &&
                                laneOffset > offset) {
                                newIndex = lane.index;
                                offset = laneOffset;
                                right = true;
                                return true;
                            }
                            return false;
                        });
                    }

                    var left = false;
                    var right = false;
                    GAME.race.track.lanes.forEach((lane)=>{
                       if (lane.index != newIndex ) {
                           if (lane.distanceFromCenter < GAME.getLaneOffset(GAME.thisPosition.piecePosition.lane.startLaneIndex))
                           {
                               left = true;
                           } else {
                               right = true;
                           }
                       }
                    });
                    var blocked = false;
                    if (GAME.inFront && GAME.inFront.length > 0) {
                        blocked = GAME.inFront.some((op):boolean=>{
                            if (op.lane == newIndex) {
                                if (op.distance < toNextSwitch &&
                                    GAME.speed - op.speed > -0.1) {
                                    return true;
                                }
                            }
                            return false;
                        });
                    }
                    if (blocked) {
                        if (left) {
                            direction = "Left";
                        } else if (right) {
                            direction = "Right";
                        }
                        GAME.state = "Maybe blocked switching: " + direction;
                    } else {
                        GAME.state = "Next piece is a switch piece, switching " + direction;
                    }
                    if (!direction)
                        return;
                    this.send(new Message('switchLane', direction));
                    rec.handled = true;
                    rec.replied = true;
                    this.lastIndex = GAME.thisPosition.piecePosition.pieceIndex;
                }
            }
        }
	}

}