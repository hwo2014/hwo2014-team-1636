/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./game"/>

declare var GAME:game.GameState;

module bot {

	import Message = comms.Message;

	export class TurboBot extends AbstractBot {

        startTurboBeforeStraight:number = 30;
        requiredTurboDistance:number = 400;

        gameInit(msg:Message):void {
            console.log("TurboBot overriding requiredTurboDistance based on track");
            this.requiredTurboDistance = GAME.longestStraightPiece - 20;
            console.log("  Setting to:" + this.requiredTurboDistance);
        }

		carPositions(msg:Message):void {
            if (msg.handled || !GAME.turbo ||
                GAME.crashed)
                return;

            if (GAME.longestStraightPieceIndexes.indexOf(GAME.thisPosition.piecePosition.pieceIndex + 1) == -1)
                return;

            var toNextPiece:number = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;
            if (GAME.thisPiece.angle && toNextPiece > this.startTurboBeforeStraight)
                return;

            GAME.state = "TURBO!";
            console.log("TURBOOOOOOOOOOO:", GAME.distanceToCorner);
            this.send(new Message('turbo', 'Kiss your butt good bye'));
            GAME.turbo = null;
            msg.handled = true;
            msg.replied = true;
        }
	}

}
