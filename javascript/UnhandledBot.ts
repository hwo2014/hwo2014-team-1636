/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>

module bot {

	import Message = comms.Message;

	export class UnhandledBot extends AbstractBot {


        handleMessage(msg:Message):void {
            // reply to the server
            if (!msg.handled)
                console.log('\n--Message: ' + msg.msgType + ' : ', msg.data);
            msg.handled = true;
            if (!msg.replied) {
//                if ((msg.msgType == 'gameStart' || msg.msgType == 'carPositions' ||
//                    msg.msgType == 'gameInit')) {
                    this.ping();
//                }
            }
            msg.replied = true;
        }
	}

}
