
module base {

	export class Point {
		constructor(
			public x?:number,
			public y?:number
		) {}
	}
	export class Pose {
		position:Point;
		angle:number;
		constructor();
		constructor(position:Point, angle?:number)
		constructor(
			xOrPosition?:any,
			yOrAngle?:number,
			angle?:number
		) {
			if (xOrPosition === null) {
				this.position = new Point();
				this.angle = 0;
			} else if (typeof xOrPosition === "number") {
				xOrPosition = xOrPosition | 0;
				yOrAngle = yOrAngle | 0;
				angle = angle | 0;
				this.position = new Point(xOrPosition, yOrAngle);
				this.angle = angle;
			} else {
				yOrAngle = yOrAngle | 0;
				this.position = xOrPosition;
				this.angle = yOrAngle;
			}
		}
	}
}