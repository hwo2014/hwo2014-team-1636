@echo off

setlocal

for /f "tokens=2,3 delims== " %%i in (..\config) do set %%i=%%j

if "offline" == "%BUILD_MODE%" goto offline

call npm install
goto end

:offline
  echo "offline build"
  goto end

:end
  echo building main
  call tsc --target ES5 --out index.js --module commonjs main.ts

  echo building trial
  call tsc --target ES5 --out trial.js --module commonjs trial.ts

  endlocal
