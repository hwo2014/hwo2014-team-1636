/// <reference path="./race"/>
module car {
	export class Car {
        id:race.CarId;
        dimensions:Dimensions;
		constructor() {}
	}

    export class Dimensions {
        length:number;
        width:number;
        guideFlagPosition:number;
    }

    export class Turbo {
        turboDurationMillisecons:number;
        turboDurationTicks:number;
        turboFactor:number;
    }
}