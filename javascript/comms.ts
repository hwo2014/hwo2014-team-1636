/// <reference path="./base"/>

declare function require(any):any;

module comms {

	var net:any = require('net');
	var JSONStream:any = require('JSONStream');

	export class Comms {
		client:any;
		jsonStream:any;

		constructor(
			public serverHost:string, 
			public serverPort:string, 
			public botName:string, 
			public botKey:string,
			public handlers:IMessageHandler[]
		) {
			handlers.forEach(function(handler) {
//                console.log("Comms: ", this);
				handler.server = this;
			}, this);
		}

		connect(callback:()=>boolean):any {

			console.log("I'm", this.botName, "and connect to", this.serverHost + ":" + this.serverPort);

			this.client = net.connect(this.serverPort, this.serverHost, callback);

			this.jsonStream = this.client.pipe(JSONStream.parse());
            server = this;
			this.jsonStream.on('data', function(data) {
//                console.log("Message", data);
				var msg:Message = data;//new Message(data.msgType, data.data);
				server.handlers.forEach(function(handler) {
					handler.handleMessage(msg);
				});

			});

			this.jsonStream.on('error', function() {
			    return console.log("disconnected");
			});
		}

		send(json:any, debug:boolean = false):boolean {
            if (debug)
                console.log("msg before json: ", json);
            var msg:string = JSON.stringify(json, (key:string, value:any)=>{
                if (value == null)
                    return undefined;
                if (key === 'handled' || key === 'replied')
                    return undefined;
                return value;
            });
            if (debug)
                console.log("sending msg", msg);

		    this.client.write(msg);
		    return this.client.write('\n');
		}

		join():boolean {
			return this.send(new Join(this.botKey, this.botName));
		}

//		createRace(trackName:string, password?:string, carCount?:number):void {
//			carCount = carCount || 1;
//			this.send(new CreateRace(this.botKey, this.botName,
//				trackName, password, carCount|1));
//		}

		joinRace(trackName:string, carCount?:number, password?:string):boolean {
			carCount = carCount || 1;
			return this.send(new CreateRace(this.botKey, this.botName,
				trackName, password, carCount, 'joinRace'), true);
		}
	}

	export class Message {
        handled:boolean = false;
        replied:boolean = false;
        gameTick:number;
        gameId:string;
		constructor(
			public msgType:string,
			public data?:any
		) {}
	}

	export interface IMessageHandler {
		server:Comms

		handleMessage(message:Message):void
	}

	export class Ping extends Message {
		constructor() {
			super('ping', null);
		}
	}

	export class Join extends Message {

		constructor(
			botKey:string,
			botName:string
		) {
			super('join', {
                key: botKey,
                name: botName
            });
		}

	}

	export class CreateRace extends Message {
		constructor (
			botKey:string,
			botName:string,
			trackName:string,
			password:string,
			carCount:number,
			msgType?:string
		) {
			super(msgType || 'createRace',
                {
                    botId: {
                        name: botName,
                        key: botKey
                    },
                    trackName: trackName,
                    password: password,
                    carCount: carCount
                });
            console.log("Car Count type: ", typeof carCount);
		}
	}
}