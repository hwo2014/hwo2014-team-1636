
var cp = require('child_process');
var numCPUs = require('os').cpus().length;
var fs = require('fs');
var path = require('path');

var queue = [];

if (process.argv.length < 3) {
	console.log("Please provide a directory or file to look through e.g. node convert.js c:\\logs and it will process all the .txt files to stdout");
	process.exit();
}

var doNext = function() {
	process.nextTick(function() {
		if (queue.length > 0)
			processFile(queue.shift());
	});
}

var dir = process.argv[2];

dir = path.normalize(path.resolve(dir));

var processFile = function(file) {

// var LineByLineReader = require('line-by-line');
var byline = require('byline');

var inMessage = false;
var msg = '';

var laps = [];
var track = "";
var args = null;
// var file = process.argv[2];
var logged = false;
var rs = fs.createReadStream(dir + file, {encoding: 'utf8'});
rs.on('error', function(err) {
	console.log('Stream error', err);
});
var lr = byline(rs);
var lines = 0;

lr.on('error', function(err) {
	console.log("Error:", err);
});

lr.on('data', function(line) {
	lines++;
	if (!args && line.indexOf("cd ..") == 0) {
		args = line.substr(69).split(" ");
		track = args.shift();
		args = args.map(function(arg){return parseFloat(arg)});
		return;
	}
	var firstChar = line.charAt(0);
	if (firstChar == '-') {
		inMessage = true;
		msg = line.substr(11);
	} else if (inMessage && (firstChar == ' ' || firstChar == '\t')) {
		msg += ' ' + line;
	} else {
		if (inMessage) {
			// console.log(msg);
			var parsedMsg;
			try {
				msg = '{' + msg + '}';
				msg = msg.replace(/'/g, "\"");
				msg = msg.replace(" : ", ": ");
				parsedMsg = JSON.parse(msg.replace(/(['"])?([a-zA-Z0-9_]+)(['"])?:/g, '"$2": '));
			} catch (e) {
			}

			if (parsedMsg && parsedMsg.lapFinished) {
				// console.log(parsedMsg.lapFinished.lapTime.lap, parsedMsg.lapFinished.lapTime.millis)
				laps[parsedMsg.lapFinished.lapTime.lap] = parsedMsg.lapFinished.lapTime.millis;
			}
		}
		inMessage = false;
	}
});
lr.on('end', function() {
	// if (logged)
	// 	return;

	// console.log("lines", lines);
	logged = true;
	console.log(file + ',' + track + ',' + args.concat(laps).join(","));

	doNext();
	// results.push(track + ',' + args.concat(laps).join(","));
	// console.log("Laps", laps);
});

//=IF(OR(ISBLANK(I2),ISBLANK(J2),ISBLANK(K2)), "DNF", "")
// }
};

console.log("File,Track,Cars,TurboCorner,TurboStraight,Brake Force,Brake Offset,Brake Effect,Reactive Brake,Lap 1, Lap 2, Lap 3, (Lap x...)");
stat = fs.statSync(dir);
if (stat.isFile()) {

	queue = [path.basename(dir)];
	dir = path.dirname(dir)  + path.sep;
} else {
	dir += path.sep
	queue = fs.readdirSync(dir).filter(function(name) {
		return path.extname(name) == ".txt";
	});
	
}
doNext();