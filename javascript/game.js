/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
var GameState = (function () {
    function GameState() {
    }
    GameState.prototype.getPiece = function (id) {
        return this.race.track.pieces[(id + this.race.track.pieces.length) % this.race.track.pieces.length];
    };

    GameState.prototype.getLaneOffset = function (id) {
        var result = 0;
        this.race.track.lanes.some(function (lane) {
            if (lane.index == id) {
                result = lane.distanceFromCenter;
                return true;
            }
            return false;
        }, this);
        return result;
    };
    return GameState;
})();
exports.GameState = GameState;
