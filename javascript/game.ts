/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>

module game {
    export class GameState {

        calibrating:boolean = true;
        race:race.Race;
        carColor:string;
        car:car.Car;
        carMass:number = 0;
        speed:number;
        lastSpeed:number;
        speedSec:number;
        lastSpeedSec:number;
        acceleration:number;
        accelerationSec:number;
        throttle:number = 1;
        /** This is the engine engineForce it can only be worked out once we start moving*/
        engineForce:number = 0;
        state:string = "Full throttle";
        thisPiece:track.Piece;
        lastPiece:track.Piece;
        nextCorner:track.Piece;
        thisPosition:race.CarPosition;
        thisLength:number;
        lastLength:number;
        fastestLap:number = -1;
        crashed:boolean = false;
        distanceToCorner:number;
        turbo:car.Turbo = null;
        turboOn:boolean = false;
        started:boolean = false;
        lastLap:boolean = false;
        canSeeFinish:boolean = false;
        targetCornerSpeed:number = 10;
        opponents:Object = new Object();
        inFront:race.OpponentPosition[];
        inFrontLane:race.OpponentPosition[];
        behind:race.OpponentPosition[];
        behindLane:race.OpponentPosition[];
        nextCornerRadius:number = 0;
        thisCornerRadius:number = 1000;
        longestStraightPieceIndexes:number[] = [];
        longestStraightPiece:number = 1;
        tick:number = 0;
        lastTick:number = -1;


        getBlockerDistance():number {
            if (GAME.inFrontLane.length > 0) {
                var opponent:race.OpponentPosition = GAME.inFrontLane[0];
                if (opponent.speed <= GAME.speed) {
                    return opponent.distance;
                }
            }
            return -1;
        }

        getPusherDistance():number {
            if (GAME.behindLane.length > 0) {
                var opponent:race.OpponentPosition = GAME.behindLane[0];
                if (opponent.speed >= GAME.speed) {
                    return opponent.distance;
                }
            }
            return -1;
        }

        getPiece(id:number):track.Piece {
            return this.race.track.pieces[(id + this.race.track.pieces.length) % this.race.track.pieces.length];
        }

        getLaneOffset(id:number):number {
            var result:number = 0;
            this.race.track.lanes.some(function (lane:track.Lane):boolean {
                if (lane.index == id) {
                    result = lane.distanceFromCenter;
                    return true;
                }
                return false;
            }, this);
            return result;
        }

        findNextCorner(id:number):track.Piece {

            for (var i = id; i < GAME.race.track.pieces.length; i++) {
                var piece = GAME.race.track.pieces[i];
                if (piece.radius)
                    return piece;
            }

            return this.findNextCorner(0);
        }

        findNextSwitchId(id:number):number {
            for (var i = id; i < GAME.race.track.pieces.length; i++) {
                var piece = GAME.race.track.pieces[i];
                if (piece.switch == true)
                    return i;
            }

            return 8;
        }

        getDistanceToNextCornerPiece():number {
            var result:number = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;
//            GAME.nextCorner = null;
//            if (GAME.thisPiece.angle)
//                return 0;
            var piece:track.Piece;
            var id:number = GAME.thisPosition.piecePosition.pieceIndex + 1;
            while (true) {
                piece = GAME.getPiece(id);
                if (piece.angle) {
                    GAME.nextCorner = piece;
                    break;
                }
                result += this.getLength(piece);
                id++;
            }
            return result;
        }

        getLength(piece:track.Piece, pos?:race.CarPosition):number {
            pos = pos || GAME.thisPosition;
            if (piece.length) {
                var startLaneOffset:number = GAME.getLaneOffset(pos.piecePosition.lane.startLaneIndex);
                var endLaneOffset:number = GAME.getLaneOffset(pos.piecePosition.lane.endLaneIndex);
                var length:number = piece.length;
                if (startLaneOffset != endLaneOffset) {
                    var delta:number = Math.abs(startLaneOffset - endLaneOffset);
                    length = Math.sqrt(length * length + delta * delta);
                }
                return length;
            } else {
                var startLaneOffset:number = GAME.getLaneOffset(pos.piecePosition.lane.startLaneIndex);
                var endLaneOffset:number = GAME.getLaneOffset(pos.piecePosition.lane.endLaneIndex);
                var radius = piece.radius;
                if (piece.angle < 0) {
                    radius += startLaneOffset;
                } else {
                    radius -= startLaneOffset;
                }
                var length:number =  Math.abs((2 * Math.PI * radius) * (piece.angle / 360));
                if (startLaneOffset != endLaneOffset)
                {
                    radius = piece.radius;
                    if (piece.angle < 0) {
                        radius += endLaneOffset;
                    } else {
                        radius -= endLaneOffset;
                    }
                    var endLength:number =  Math.abs((2 * Math.PI * radius) * (piece.angle / 360));
                    length = length / 2 + endLength / 2;
                }
                return length;
            }
        }

        getDelta(a:race.CarPosition, b:race.CarPosition):number {
            var first:race.CarPosition;
            var second:race.CarPosition;
            if (!a || !b) {
                return 0;
            }
            if (a.piecePosition.lap > b.piecePosition.lap) {
                first = a;
                second = b;
            } else if (a.piecePosition.lap < b.piecePosition.lap) {
                first = b;
                second = a;
            } else if (a.piecePosition.inPieceDistance > b.piecePosition.inPieceDistance) {
                first = a;
                second = b;
            } else {
                second = b;
                first = a;
            }
            var result:number = GAME.thisLength - first.piecePosition.inPieceDistance;
            var piece:track.Piece;
            var id:number = first.piecePosition.pieceIndex + 1;
            while (id < second.piecePosition.pieceIndex) {
                piece = GAME.getPiece(id);
                result += this.getLength(piece);
                id++;
            }
            result += second.piecePosition.inPieceDistance;
            if (a === first) {
                return result;
            } else {
                return -result;
            }
        }
    }
}
