var base;
(function (base) {
    var Point = (function () {
        function Point(x, y) {
            this.x = x;
            this.y = y;
        }
        return Point;
    })();
    base.Point = Point;
    var Pose = (function () {
        function Pose(xOrPosition, yOrAngle, angle) {
            if (xOrPosition === null) {
                this.position = new Point();
                this.angle = 0;
            } else if (typeof xOrPosition === "number") {
                xOrPosition = xOrPosition | 0;
                yOrAngle = yOrAngle | 0;
                angle = angle | 0;
                this.position = new Point(xOrPosition, yOrAngle);
                this.angle = angle;
            } else {
                yOrAngle = yOrAngle | 0;
                this.position = xOrPosition;
                this.angle = yOrAngle;
            }
        }
        return Pose;
    })();
    base.Pose = Pose;
})(base || (base = {}));
/// <reference path="./base"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};

var comms;
(function (comms) {
    var net = require('net');
    var JSONStream = require('JSONStream');

    var Comms = (function () {
        function Comms(serverHost, serverPort, botName, botKey, bot) {
            this.serverHost = serverHost;
            this.serverPort = serverPort;
            this.botName = botName;
            this.botKey = botKey;
            this.bot = bot;
            bot.server = this;
        }
        Comms.prototype.connect = function (callback) {
            console.log("I'm", this.botName, "and connect to", this.serverHost + ":" + this.serverPort);

            this.client = net.connect(this.serverPort, this.serverHost, callback);

            this.jsonStream = this.client.pipe(JSONStream.parse());

            this.jsonStream.on('data', function (data) {
                var msg = new Message(data.msgType, data.data);
                this.bot.handleMessage(msg);
            });

            this.jsonStream.on('error', function () {
                return console.log("disconnected");
            });
        };

        Comms.prototype.send = function (json) {
            this.client.write(JSON.stringify(json));
            return this.client.write('\n');
        };

        Comms.prototype.join = function () {
            this.send(new Join(this.botKey, this.botName));
        };
        return Comms;
    })();
    comms.Comms = Comms;

    var Message = (function () {
        function Message(msgType, data) {
            this.msgType = msgType;
            this.data = data;
        }
        return Message;
    })();
    comms.Message = Message;

    var Ping = (function (_super) {
        __extends(Ping, _super);
        function Ping() {
            _super.call(this, 'ping', null);
        }
        return Ping;
    })(Message);
    comms.Ping = Ping;

    var Join = (function (_super) {
        __extends(Join, _super);
        function Join(botKey, botName) {
            _super.call(this, 'join');
            this.botKey = botKey;
            this.botName = botName;
        }
        Object.defineProperty(Join.prototype, "data", {
            get: function () {
                return {
                    botKey: botKey,
                    botName: botName
                };
            },
            enumerable: true,
            configurable: true
        });
        return Join;
    })(Message);
    comms.Join = Join;
})(comms || (comms = {}));
/// <reference path="./comms"/>
var bot;
(function (bot) {
    var Ping = comms.Ping;

    var AbstractBot = (function () {
        function AbstractBot(server) {
            this.server = server;
        }
        AbstractBot.prototype.handleMessage = function (msg) {
            if (typeof this[msg.msgType] === 'function') {
                this[msg.msgType](msg);
            } else {
                console.log('Unknown message: ' + msg.msgType + ' : ', msg.data);
                this.ping();
            }
        };

        AbstractBot.prototype.send = function (msg) {
            this.server.send(msg);
        };

        AbstractBot.prototype.ping = function () {
            this.send(new Ping());
        };
        return AbstractBot;
    })();
    bot.AbstractBot = AbstractBot;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
var bot;
(function (bot) {
    var Message = comms.Message;

    var SimpleBot = (function (_super) {
        __extends(SimpleBot, _super);
        function SimpleBot() {
            _super.apply(this, arguments);
        }
        SimpleBot.prototype.carPositions = function (rec) {
            this.send(new Message('throttle', 0.5));
        };

        SimpleBot.prototype.join = function (rec) {
            console.log('Joined');
            this.ping();
        };

        SimpleBot.prototype.gameStart = function (rec) {
            console.log('Race started');
            this.ping();
        };

        SimpleBot.prototype.gameEnd = function (rec) {
            console.log('Race ended');
            this.ping();
        };
        return SimpleBot;
    })(bot.AbstractBot);
    bot.SimpleBot = SimpleBot;
})(bot || (bot = {}));
/// <reference path="./comms" />
/// <reference path="./SimpleBot" />

var Comms = comms.Comms;

var SimpleBot = bot.SimpleBot;

var serverHost = "testserver.helloworldopen.com";
var serverPort = "8091";
var botName = "797F";
var botKey = "i237nvaJ2cPdOQ";

var ai = new SimpleBot();

var server = new comms.Comms(serverHost, serverPort, botName, botKey, ai);

server.connect(function () {
    server.join();
});
