/// <reference path="./comms" />
/// <reference path="./race" />
/// <reference path="./game" />
/// <reference path="./SimpleBot.ts" />
/// <reference path="./NicBot1" />
/// <reference path="./NicBot2" />
/// <reference path="./SimpleBot" />
/// <reference path="./FasterBot" />
/// <reference path="./FasterBotNic1.ts" />
/// <reference path="./DumbBotNic.ts" />
/// <reference path="./RaceBot" />
/// <reference path="./SwitchBot" />
/// <reference path="./GlenBot" />
/// <reference path="./LoggingBot" />
/// <reference path="./UnhandledBot" />
/// <reference path="./OpponentBot" />
/// <reference path="./TurboBot" />


declare var process:any;

import Comms = comms.Comms;
import IMessageHandler = comms.IMessageHandler;

var serverHost:string = "testserver.helloworldopen.com";
var GAME:game.GameState = new game.GameState();
var TRIAL:boolean = false;
//var serverHost:string = "webber.helloworldopen.com";
var serverPort:string = "8091";
var botName:string = "797F";
var botKey:string = "i237nvaJ2cPdOQ";
var t:string = null;
var numCars:number = 1;

if (process.argv.length > 1)
    serverHost = process.argv[2];
if (process.argv.length > 2)
    serverPort = process.argv[3];
if (process.argv.length > 3)
    botName = process.argv[4];
if (process.argv.length > 4)
    botKey = process.argv[5];
if (process.argv.length > 5)
    t = process.argv[6];
if (process.argv.length > 6)
    numCars = parseInt(process.argv[7]);

if (t)
    botName = "Glen" + Math.floor(10 * Math.random());

//var ai:bot.FasterBotNic1 = new bot.FasterBotNic1();
//var ai:bot.NicBot2 = new bot.NicBot2();
var ai:bot.GlenBot = new bot.GlenBot();
//var ai:bot.DumbBotNic = new bot.DumbBotNic();

var switchBot:bot.SwitchBot = new bot.SwitchBot();
var raceBot:bot.RaceBot = new bot.RaceBot();
var log:bot.LoggingBot = new bot.LoggingBot();
var unhandled:bot.UnhandledBot = new bot.UnhandledBot();
var turboBot:bot.TurboBot = new bot.TurboBot();
var opponentBot:bot.OpponentBot = new bot.OpponentBot();

// switchBot, turboBot,
var server:Comms = new Comms(
    serverHost, serverPort, botName, botKey,
    [raceBot, opponentBot, switchBot, turboBot, ai, log, unhandled]);

server.connect(function ():boolean {
    console.log("Connected");
    if (t) {
        console.log("Joining with " + numCars + " cars");
        return server.joinRace(t,numCars,'abc');
    } else {
        return server.join();
    }
});

// keimola
// germany
// usa


function trace(anything) {
    console.log(anything);
}
