/// <reference path="./track"/>
/// <reference path="./car"/>

module race {

    import Track = track.Track;
    import Car = car.Car;

	export class Race {
		constructor(
			public track:Track,
			public cars:Car[],
			public raceSession:RaceSession
		) {}
	}

	export class RaceSession {
		constructor(
			public laps:number,
			public maxLapTimeMs:number,
			public quickRace:boolean
		) {}
	}

    export class CarPosition {
        id: CarId;
        angle:number;
        piecePosition:PiecePosition;
    }

    export class CarId {
        name:string;
        color:string;
    }

    export class PiecePosition {
        pieceIndex:number;
        inPieceDistance:number;
        lane:LanePosition;
        lap:number;
    }

    export class LanePosition {
        startLaneIndex:number;
        endLaneIndex:number;
    }

    export class OpponentPosition {
        lane:number;
        distance:number;
        speed:number;
        lastPosition:race.CarPosition;
        bestLap:number = Number.MAX_VALUE;

        static distanceSort(a:OpponentPosition, b:OpponentPosition):number {
            return a.distance - b.distance;
        }
    }
}