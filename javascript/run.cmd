@echo off

setlocal

call ..\scripts\check_pl_args %*

if ERRORLEVEL 1 goto end

call node index.js %*

:end

endlocal