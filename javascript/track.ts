/// <reference path="./base"/>

import Pose = base.Pose

module track {
	export class Track {

		constructor(
			public id:string,
			public name:string,
			public pieces:Piece[],
			public lanes:Lane[],
			public startingPoint:StartingPoint
		) {}
	}

	export class Piece {
		length:number;
		switch:boolean;
		radius:number;
		angle:number;
	}

	export class Lane {
		index:number;
		distanceFromCenter:number;
	}

	export class StartingPoint extends Pose {
		
	}
}