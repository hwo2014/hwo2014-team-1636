var base;
(function (base) {
    var Point = (function () {
        function Point(x, y) {
            this.x = x;
            this.y = y;
        }
        return Point;
    })();
    base.Point = Point;
    var Pose = (function () {
        function Pose(xOrPosition, yOrAngle, angle) {
            if (xOrPosition === null) {
                this.position = new Point();
                this.angle = 0;
            } else if (typeof xOrPosition === "number") {
                xOrPosition = xOrPosition | 0;
                yOrAngle = yOrAngle | 0;
                angle = angle | 0;
                this.position = new Point(xOrPosition, yOrAngle);
                this.angle = angle;
            } else {
                yOrAngle = yOrAngle | 0;
                this.position = xOrPosition;
                this.angle = yOrAngle;
            }
        }
        return Pose;
    })();
    base.Pose = Pose;
})(base || (base = {}));
/// <reference path="./base"/>
var __extends = this.__extends || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    __.prototype = b.prototype;
    d.prototype = new __();
};

var comms;
(function (comms) {
    var net = require('net');
    var JSONStream = require('JSONStream');

    var Comms = (function () {
        function Comms(serverHost, serverPort, botName, botKey, handlers) {
            this.serverHost = serverHost;
            this.serverPort = serverPort;
            this.botName = botName;
            this.botKey = botKey;
            this.handlers = handlers;
            handlers.forEach(function (handler) {
                //                console.log("Comms: ", this);
                handler.server = this;
            }, this);
        }
        Comms.prototype.connect = function (callback) {
            console.log("I'm", this.botName, "and connect to", this.serverHost + ":" + this.serverPort);

            this.client = net.connect(this.serverPort, this.serverHost, callback);

            this.jsonStream = this.client.pipe(JSONStream.parse());
            server = this;
            this.jsonStream.on('data', function (data) {
                //console.log("Message", data);
                var msg = new Message(data.msgType, data.data);
                server.handlers.forEach(function (handler) {
                    handler.handleMessage(msg);
                });
            });

            this.jsonStream.on('error', function () {
                return console.log("disconnected");
            });
        };

        Comms.prototype.send = function (json, debug) {
            if (typeof debug === "undefined") { debug = false; }
            if (debug)
                console.log("sending msg", json);

            this.client.write(JSON.stringify(json));
            return this.client.write('\n');
        };

        Comms.prototype.join = function () {
            return this.send(new Join(this.botKey, this.botName));
        };

        //		createRace(trackName:string, password?:string, carCount?:number):void {
        //			carCount = carCount || 1;
        //			this.send(new CreateRace(this.botKey, this.botName,
        //				trackName, password, carCount|1));
        //		}
        Comms.prototype.joinRace = function (trackName, carCount, password) {
            carCount = carCount || 1;
            return this.send(new CreateRace(this.botKey, this.botName, trackName, password, carCount, 'joinRace'), true);
        };
        return Comms;
    })();
    comms.Comms = Comms;

    var Message = (function () {
        function Message(msgType, data) {
            this.msgType = msgType;
            this.data = data;
            this.handled = false;
            this.replied = false;
        }
        return Message;
    })();
    comms.Message = Message;

    var Ping = (function (_super) {
        __extends(Ping, _super);
        function Ping() {
            _super.call(this, 'ping', null);
        }
        return Ping;
    })(Message);
    comms.Ping = Ping;

    var Join = (function (_super) {
        __extends(Join, _super);
        function Join(botKey, botName) {
            _super.call(this, 'join', {
                key: botKey,
                name: botName
            });
        }
        return Join;
    })(Message);
    comms.Join = Join;

    var CreateRace = (function (_super) {
        __extends(CreateRace, _super);
        function CreateRace(botKey, botName, trackName, password, carCount, msgType) {
            _super.call(this, msgType || 'createRace', {
                botId: {
                    name: botName,
                    key: botKey
                },
                trackName: trackName,
                password: password,
                carCount: carCount
            });
            this.botKey = botKey;
            this.botName = botName;
            this.trackName = trackName;
            this.password = password;
            this.carCount = carCount;
        }
        return CreateRace;
    })(Message);
    comms.CreateRace = CreateRace;
})(comms || (comms = {}));
/// <reference path="./base"/>
var Pose = base.Pose;

var track;
(function (track) {
    var Track = (function () {
        function Track(id, name, pieces, lanes, startingPoint) {
            this.id = id;
            this.name = name;
            this.pieces = pieces;
            this.lanes = lanes;
            this.startingPoint = startingPoint;
        }
        return Track;
    })();
    track.Track = Track;

    var Piece = (function () {
        function Piece() {
        }
        return Piece;
    })();
    track.Piece = Piece;

    var Lane = (function () {
        function Lane() {
        }
        return Lane;
    })();
    track.Lane = Lane;

    var StartingPoint = (function (_super) {
        __extends(StartingPoint, _super);
        function StartingPoint() {
            _super.apply(this, arguments);
        }
        return StartingPoint;
    })(Pose);
    track.StartingPoint = StartingPoint;
})(track || (track = {}));
/// <reference path="./race"/>
var car;
(function (car) {
    var Car = (function () {
        function Car() {
        }
        return Car;
    })();
    car.Car = Car;

    var Dimensions = (function () {
        function Dimensions() {
        }
        return Dimensions;
    })();
    car.Dimensions = Dimensions;

    var Turbo = (function () {
        function Turbo() {
        }
        return Turbo;
    })();
    car.Turbo = Turbo;
})(car || (car = {}));
/// <reference path="./track"/>
/// <reference path="./car"/>
var race;
(function (race) {
    var Race = (function () {
        function Race(track, cars, raceSession) {
            this.track = track;
            this.cars = cars;
            this.raceSession = raceSession;
        }
        return Race;
    })();
    race.Race = Race;

    var RaceSession = (function () {
        function RaceSession(laps, maxLapTimeMs, quickRace) {
            this.laps = laps;
            this.maxLapTimeMs = maxLapTimeMs;
            this.quickRace = quickRace;
        }
        return RaceSession;
    })();
    race.RaceSession = RaceSession;

    var CarPosition = (function () {
        function CarPosition() {
        }
        return CarPosition;
    })();
    race.CarPosition = CarPosition;

    var CarId = (function () {
        function CarId() {
        }
        return CarId;
    })();
    race.CarId = CarId;

    var PiecePosition = (function () {
        function PiecePosition() {
        }
        return PiecePosition;
    })();
    race.PiecePosition = PiecePosition;

    var LanePosition = (function () {
        function LanePosition() {
        }
        return LanePosition;
    })();
    race.LanePosition = LanePosition;

    var OpponentPosition = (function () {
        function OpponentPosition() {
            this.bestLap = Number.MAX_VALUE;
        }
        OpponentPosition.distanceSort = function (a, b) {
            return a.distance - b.distance;
        };
        return OpponentPosition;
    })();
    race.OpponentPosition = OpponentPosition;
})(race || (race = {}));
/// <reference path="./comms"/>
/// <reference path="./race"/>
var bot;
(function (bot) {
    var Ping = comms.Ping;

    var AbstractBot = (function () {
        function AbstractBot(server) {
            this.server = server;
        }
        AbstractBot.prototype.handleMessage = function (msg) {
            if (typeof this[msg.msgType] === 'function') {
                this[msg.msgType](msg);
            }
        };

        AbstractBot.prototype.send = function (msg) {
            this.server.send(msg);
        };

        AbstractBot.prototype.ping = function () {
            this.send(new Ping());
        };
        return AbstractBot;
    })();
    bot.AbstractBot = AbstractBot;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
var game;
(function (game) {
    var GameState = (function () {
        function GameState() {
            this.carMass = 0;
            this.throttle = 1;
            /** This is the engine engineForce it can only be worked out once we start moving*/
            this.engineForce = 0;
            this.state = "Full throttle";
            this.fastestLap = -1;
            this.crashed = false;
            this.turbo = null;
            this.turboOn = false;
            this.started = false;
            this.lastLap = false;
            this.canSeeFinish = false;
            this.targetCornerSpeed = 10;
            this.opponents = new Object();
            this.nextCornerRadius = 0;
            this.longestStraightPieceIndexes = [];
            this.longestStraightPiece = 1;
        }
        GameState.prototype.getBlockerDistance = function () {
            if (GAME.inFrontLane.length > 0) {
                var opponent = GAME.inFrontLane[0];
                if (opponent.speed <= GAME.speed) {
                    return opponent.distance;
                }
            }
            return -1;
        };

        GameState.prototype.getPusherDistance = function () {
            if (GAME.behindLane.length > 0) {
                var opponent = GAME.behindLane[0];
                if (opponent.speed >= GAME.speed) {
                    return opponent.distance;
                }
            }
            return -1;
        };

        GameState.prototype.getPiece = function (id) {
            return this.race.track.pieces[(id + this.race.track.pieces.length) % this.race.track.pieces.length];
        };

        GameState.prototype.getLaneOffset = function (id) {
            var result = 0;
            this.race.track.lanes.some(function (lane) {
                if (lane.index == id) {
                    result = lane.distanceFromCenter;
                    return true;
                }
                return false;
            }, this);
            return result;
        };

        GameState.prototype.findNextCorner = function (id) {
            for (var i = id; i < GAME.race.track.pieces.length; i++) {
                var piece = GAME.race.track.pieces[i];
                if (piece.radius)
                    return piece;
            }

            return this.findNextCorner(0);
        };

        GameState.prototype.findNextSwitchId = function (id) {
            for (var i = id; i < GAME.race.track.pieces.length; i++) {
                var piece = GAME.race.track.pieces[i];
                if (piece.switch == true)
                    return i;
            }

            return 8;
        };

        GameState.prototype.getDistanceToNextCornerPiece = function () {
            var result = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;

            //            GAME.nextCorner = null;
            //            if (GAME.thisPiece.angle)
            //                return 0;
            var piece;
            var id = GAME.thisPosition.piecePosition.pieceIndex + 1;
            while (true) {
                piece = GAME.getPiece(id);
                if (piece.angle) {
                    GAME.nextCorner = piece;
                    break;
                }
                result += this.getLength(piece);
                id++;
            }
            return result;
        };

        GameState.prototype.getLength = function (piece, pos) {
            pos = pos || GAME.thisPosition;
            if (piece.length) {
                var startLaneOffset = GAME.getLaneOffset(pos.piecePosition.lane.startLaneIndex);
                var endLaneOffset = GAME.getLaneOffset(pos.piecePosition.lane.endLaneIndex);
                var length = piece.length;
                if (startLaneOffset != endLaneOffset) {
                    var delta = Math.abs(startLaneOffset - endLaneOffset);
                    length = Math.sqrt(length * length + delta * delta);
                }
                return length;
            } else {
                var startLaneOffset = GAME.getLaneOffset(pos.piecePosition.lane.startLaneIndex);
                var endLaneOffset = GAME.getLaneOffset(pos.piecePosition.lane.endLaneIndex);
                var radius = piece.radius;
                if (piece.angle < 0) {
                    radius += startLaneOffset;
                } else {
                    radius -= startLaneOffset;
                }
                var length = Math.abs((2 * Math.PI * radius) * (piece.angle / 360));
                if (startLaneOffset != endLaneOffset) {
                    radius = piece.radius;
                    if (piece.angle < 0) {
                        radius += endLaneOffset;
                    } else {
                        radius -= endLaneOffset;
                    }
                    var endLength = Math.abs((2 * Math.PI * radius) * (piece.angle / 360));
                    length = length / 2 + endLength / 2;
                }
                return length;
            }
        };

        GameState.prototype.getDelta = function (a, b) {
            var first;
            var second;
            if (a.piecePosition.lap > b.piecePosition.lap) {
                first = a;
                second = b;
            } else if (a.piecePosition.lap < b.piecePosition.lap) {
                first = b;
                second = a;
            } else if (a.piecePosition.inPieceDistance > b.piecePosition.inPieceDistance) {
                first = a;
                second = b;
            } else {
                second = b;
                first = a;
            }
            var result = GAME.thisLength - first.piecePosition.inPieceDistance;
            var piece;
            var id = first.piecePosition.pieceIndex + 1;
            while (id < second.piecePosition.pieceIndex) {
                piece = GAME.getPiece(id);
                result += this.getLength(piece);
                id++;
            }
            result += second.piecePosition.inPieceDistance;
            if (a === first) {
                return result;
            } else {
                return -result;
            }
        };
        return GameState;
    })();
    game.GameState = GameState;
})(game || (game = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
var bot;
(function (bot) {
    var Message = comms.Message;

    var SimpleBot = (function (_super) {
        __extends(SimpleBot, _super);
        function SimpleBot() {
            _super.apply(this, arguments);
        }
        SimpleBot.prototype.carPositions = function (rec) {
            this.send(new Message('throttle', 0.6));
            rec.handled = true;
            rec.replied = true;
        };
        return SimpleBot;
    })(bot.AbstractBot);
    bot.SimpleBot = SimpleBot;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
var bot;
(function (bot) {
    var Message = comms.Message;

    var NicBot1 = (function (_super) {
        __extends(NicBot1, _super);
        function NicBot1() {
            _super.apply(this, arguments);
            this.velocity = 0.7;
            this.straightline = false;
            this.tick = 1;
            this.crashed = false;
        }
        NicBot1.prototype.join = function (rec) {
            trace('Joined');
            this.ping();
        };

        NicBot1.prototype.gameStart = function (rec) {
            trace('---------------------------------- Race started');
            trace('sending throttle');
            this.send(new Message('throttle', 1));

            this.start = new Date().getTime();
        };

        NicBot1.prototype.carPositions = function (rec) {
            //            trace(JSON.stringify(rec, null, 2));
            if (this.crashed)
                return;

            var percent = rec.data[0].piecePosition.inPieceDistance;
            var angle = rec.data[0].angle;

            trace('---------------------------------------------------------------------- ' + this.tick++ + ' / 2000 (approx)');

            trace("lap \t" + rec.data[0].piecePosition.lap + "\t " + rec.data[0].piecePosition.pieceIndex + " / 39 \t angle \t " + angle);

            if (percent > 87)
                this.velocity -= 0.45;
            else
                this.velocity += 0.25;

            if (angle > 3 || angle < -3) {
                if (this.straightline) {
                    this.velocity = 0.1;
                    this.straightline = false;
                } else {
                    this.velocity += 0.1;
                    if (this.velocity > 0.6) {
                        this.velocity = 0.6;
                    }
                }
            }

            if (angle < 3 || angle > -3) {
                if (!this.straightline) {
                    this.straightline = true;
                }
            }

            if (this.velocity > .75)
                this.velocity = .75;

            if (this.velocity < .25)
                this.velocity = .25;

            trace("VELOCITY \t" + this.velocity);

            this.send(new Message('throttle', this.velocity));
        };

        NicBot1.prototype.gameEnd = function (rec) {
            trace('----------------------------------- Race END');
            this.ping();

            var end = new Date().getTime();
            var time = end - this.start;
        };

        NicBot1.prototype.tournamentEnd = function (rec) {
            this.gameEnd(rec);
        };

        NicBot1.prototype.crash = function (rec) {
            trace(JSON.stringify(rec, null, 2));
            this.crashed = true;

            if (rec.data.name == "797F")
                setTimeout(function () {
                    process.exit();
                }, 10000);

            this.gameEnd(rec);
        };
        return NicBot1;
    })(bot.AbstractBot);
    bot.NicBot1 = NicBot1;
})(bot || (bot = {}));
/**{
"msgType": "carPositions",
"data": [
{
"id": {
"name": "797F",
"color": "red"
},
"angle": 0.045511780611135096,
"piecePosition": {
"pieceIndex": 39,
"inPieceDistance": 84.63766432298257,
"lane": {
"startLaneIndex": 0,
"endLaneIndex": 0
},
"lap": 2
}
}
]
}**/
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
var bot;
(function (bot) {
    var Message = comms.Message;

    var NicBot2 = (function (_super) {
        __extends(NicBot2, _super);
        function NicBot2() {
            _super.apply(this, arguments);
            this.velocity = 0.7;
            this.straightline = false;
            this.tick = 1;
            this.crashed = false;
        }
        NicBot2.prototype.join = function (rec) {
            trace('Joined');
            this.ping();
        };

        NicBot2.prototype.gameStart = function (rec) {
            trace('---------------------------------- Race started');
            trace('sending throttle');
            this.send(new Message('throttle', 1));

            this.start = new Date().getTime();
        };

        NicBot2.prototype.carPositions = function (rec) {
            //            trace(JSON.stringify(rec, null, 2));
            if (this.crashed)
                return;

            var percent = rec.data[0].piecePosition.inPieceDistance;
            var angle = Math.round(Number(rec.data[0].angle));

            trace('----------------------------------------------- ' + this.tick++ + ' / 2000 (approx)');

            trace("lap \t" + rec.data[0].piecePosition.lap + "\t " + rec.data[0].piecePosition.pieceIndex + " / 39 \t angle \t " + angle);

            /**
            * This gave 9:17
            */
            if (angle > 5 || angle < -5)
                this.velocity = 0.60;
            else if (angle > 4 || angle < -4)
                this.velocity = 0.625;
            else
                this.velocity = 0.65;

            if (angle == 0)
                this.velocity = 0.7;

            trace("VELOCITY \t" + this.velocity);

            this.send(new Message('throttle', this.velocity));
        };

        NicBot2.prototype.gameEnd = function (rec) {
            var end = new Date().getTime();
            var time = end - this.start;
            trace('----------------------------------- Race END');
            trace('Took ' + (time / 1000) + " seconds");
            this.ping();
        };

        NicBot2.prototype.tournamentEnd = function (rec) {
            this.gameEnd(rec);
        };

        NicBot2.prototype.crash = function (rec) {
            trace(JSON.stringify(rec, null, 2));
            this.crashed = true;

            if (rec.data.name == "797F")
                setTimeout(function () {
                    process.exit();
                }, 10000);

            this.gameEnd(rec);
        };
        return NicBot2;
    })(bot.AbstractBot);
    bot.NicBot2 = NicBot2;
})(bot || (bot = {}));
/**{
"msgType": "carPositions",
"data": [
{
"id": {
"name": "797F",
"color": "red"
},
"angle": 0.045511780611135096,
"piecePosition": {
"pieceIndex": 39,
"inPieceDistance": 84.63766432298257,
"lane": {
"startLaneIndex": 0,
"endLaneIndex": 0
},
"lap": 2
}
}
]
}**/
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./game"/>
/// <reference path="./race"/>
var bot;
(function (bot) {
    var RaceBot = (function (_super) {
        __extends(RaceBot, _super);
        function RaceBot() {
            _super.apply(this, arguments);
            this.lastPos = 0;
            this.forceFound = false;
        }
        RaceBot.prototype.yourCar = function (msg) {
            GAME.carColor = msg.data.color;
            console.log("Our car", GAME.carColor);
            msg.handled = true;
        };

        RaceBot.prototype.gameInit = function (msg) {
            GAME.race = msg.data.race;
            GAME.canSeeFinish = false;
            GAME.lastLap = false;
            var result = GAME.race.cars.some(function (car) {
                if (car.id.color == GAME.carColor) {
                    GAME.car = car;

                    //                    GAME.carMass = car.dimensions.length * car.dimensions.width;
                    console.log("car config", GAME.car.dimensions);
                    return true;
                }
                return false;
            }, this);

            this.findLongestStraightPiece();
            this.findAverageCorner();
            msg.handled = result;
        };

        RaceBot.prototype.turboAvailable = function (msg) {
            GAME.turbo = msg.data;
            msg.handled = true;
        };

        RaceBot.prototype.turboStart = function (msg) {
            msg.handled = true;
            if (msg.data.color != GAME.carColor)
                return;
            GAME.turbo = null;
            GAME.turboOn = true;
        };

        RaceBot.prototype.turboEnd = function (msg) {
            msg.handled = true;
            if (msg.data.color != GAME.carColor)
                return;
            GAME.turbo = null;
            GAME.turboOn = false;
        };

        RaceBot.prototype.carPositions = function (msg) {
            if (GAME.crashed)
                return;
            var positions = msg.data;
            positions.some(function (pos) {
                if (pos.id.color == GAME.carColor) {
                    if (GAME.thisPosition == null || pos.piecePosition.pieceIndex != GAME.thisPosition.piecePosition.pieceIndex) {
                        GAME.lastPiece = GAME.thisPiece;

                        //GAME.lastPiecePosition = GAME.thisPosition;
                        GAME.lastLength = GAME.thisLength;
                        GAME.thisPiece = GAME.getPiece(pos.piecePosition.pieceIndex);
                        if (GAME.lastPiece == null) {
                            GAME.lastPiece = GAME.getPiece(pos.piecePosition.pieceIndex - 1);
                        }
                    }
                    GAME.thisPosition = pos;
                    GAME.thisLength = GAME.getLength(GAME.thisPiece);
                    GAME.distanceToCorner = GAME.getDistanceToNextCornerPiece();
                    GAME.nextCornerRadius = this.getNextCornerRadius();
                    if (GAME.lastLap) {
                        //                        console.log("Last lap: ", GAME.race.track.pieces.indexOf(GAME.thisPiece), GAME.race.track.pieces.indexOf(GAME.nextCorner));
                        GAME.canSeeFinish = GAME.race.track.pieces.indexOf(GAME.thisPiece) > GAME.race.track.pieces.indexOf(GAME.nextCorner);
                    }

                    if (!this.forceFound) {
                        if (GAME.speed > 0) {
                            GAME.engineForce = GAME.throttle;
                            var mass = GAME.engineForce / GAME.speed;
                            if (mass > GAME.carMass) {
                                GAME.carMass = mass;
                                console.log("Car mass is ", GAME.carMass);
                            }
                        }
                    }

                    var delta = (pos.piecePosition.inPieceDistance - this.lastPos);
                    if (delta < 0) {
                        if (this.lastPos < GAME.lastLength) {
                            delta = pos.piecePosition.inPieceDistance + GAME.lastLength - this.lastPos;
                        } else {
                            // don't know how far the last piece really was...
                            console.log("Unknown piece length");
                            delta = pos.piecePosition.inPieceDistance;
                        }
                    }
                    GAME.speed = delta;

                    this.lastPos = pos.piecePosition.inPieceDistance;

                    return true;
                }
                return false;
            }, this);
        };

        RaceBot.prototype.lapFinished = function (msg) {
            if (msg.data.car.color != GAME.carColor)
                return;

            if (GAME.fastestLap == -1 || GAME.fastestLap > msg.data.lapTime.millis) {
                GAME.fastestLap = msg.data.lapTime.millis;
            }

            //            console.log("LAST LAP? ", msg.data.raceTime.laps, GAME.race.raceSession.laps);
            GAME.lastLap = msg.data.raceTime.laps == GAME.race.raceSession.laps - 1;
        };

        RaceBot.prototype.crash = function (msg) {
            if (msg.data.color != GAME.carColor)
                return;
            GAME.crashed = true;
            GAME.state = "Crashed";
            msg.handled = true;
        };

        RaceBot.prototype.spawn = function (msg) {
            if (msg.data.color != GAME.carColor)
                return;

            GAME.throttle = 1;
            GAME.turbo = null;
            GAME.turboOn = false;
            GAME.crashed = false;
            GAME.state = "Respawn";
            msg.handled = true;
        };

        RaceBot.prototype.getNextCornerRadius = function () {
            if (GAME.nextCorner.angle < 0) {
                // left turn
                return GAME.nextCorner.radius + GAME.getLaneOffset(GAME.thisPosition.piecePosition.lane.endLaneIndex);
            } else {
                // right turn
                return GAME.nextCorner.radius - GAME.getLaneOffset(GAME.thisPosition.piecePosition.lane.endLaneIndex);
            }
        };

        RaceBot.prototype.findAverageCorner = function () {
            var sumAngle = 0;
            var sumRadius = 0;
            var pieces = GAME.race.track.pieces;
            pieces.forEach(function (piece) {
                if (piece.angle) {
                    sumAngle += Math.abs(piece.angle);
                    sumRadius += piece.radius;
                }
            });
            console.log("Avg corners", (sumAngle / pieces.length).toFixed(2), (sumRadius / pieces.length).toFixed(2));
        };

        RaceBot.prototype.findLongestStraightPiece = function () {
            var result = 0;
            var bestLength = 0;
            var bestIndexes = [];
            var length = 0;
            var lastIndex = 0;
            var pieces = GAME.race.track.pieces.concat();
            var offset = 0;
            while (pieces[0].length) {
                pieces.unshift(pieces.pop());

                //                pieces.push(pieces.shift());
                offset++;
                if (offset >= pieces.length) {
                    break;
                }
            }

            //            console.log("Piece offset", offset);
            pieces.forEach(function (piece, index) {
                //               console.log("Piece",index,piece.length);
                if (piece.length) {
                    length += piece.length;
                } else {
                    if (Math.abs(length - bestLength) < 20) {
                        bestIndexes.push(lastIndex);
                        bestLength = Math.max(length, bestLength);
                    } else if (length > bestLength) {
                        bestLength = length;
                        bestIndexes.length = 0;
                        bestIndexes.push(lastIndex);
                    }
                    length = 0;
                    lastIndex = index + 1;
                }
            });
            if (Math.abs(length - bestLength) < 20) {
                bestIndexes.push(lastIndex);
                bestLength = Math.max(length, bestLength);
            } else if (length > bestLength) {
                bestLength = length;
                bestIndexes.length = 0;
                bestIndexes.push(lastIndex);
            }
            bestIndexes = bestIndexes.map(function (index) {
                return (index - offset + pieces.length) % pieces.length;
            });
            console.log("Longest straight starts at indexes: " + bestIndexes.join(',') + " and is " + bestLength + "m long");
            GAME.longestStraightPieceIndexes = bestIndexes;
            GAME.longestStraightPiece = bestLength;
        };
        return RaceBot;
    })(bot.AbstractBot);
    bot.RaceBot = RaceBot;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./RaceBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
var bot;
(function (bot) {
    var Message = comms.Message;

    var FasterBot = (function (_super) {
        __extends(FasterBot, _super);
        function FasterBot() {
            _super.apply(this, arguments);
            this.throttle = 1;
            this.lastThrottle = -1;
        }
        FasterBot.prototype.carPositions = function (rec) {
            var positions = rec.data;
            var result = positions.some(function (pos) {
                if (pos.id.name == "797F") {
                    var lastPiece = this.raceBot.getPiece(pos.piecePosition.pieceIndex - 1);
                    var thisPiece = this.raceBot.getPiece(pos.piecePosition.pieceIndex);
                    var nextPiece = this.raceBot.getPiece(pos.piecePosition.pieceIndex + 1);
                    if (lastPiece.length && thisPiece.length && nextPiece.radius && pos.piecePosition.inPieceDistance > 0.2) {
                        this.throttle = 0.3;
                    } else if (nextPiece.radius && pos.piecePosition.inPieceDistance > 0.5) {
                        this.throttle = 0.5;
                    } else if (thisPiece.length) {
                        this.throttle += 0.1;
                    }

                    if (this.throttle > 1) {
                        this.throttle = 1;
                    }
                    if (this.throttle < 0.001) {
                        this.throttle = 0.001;
                    }

                    if (this.throttle != this.lastThrottle) {
                        this.send(new Message('throttle', this.throttle));
                        this.lastThrottle = this.throttle;
                    }

                    return true;
                }
                return false;
            }, this);
            if (result) {
                rec.handled = true;
                rec.replied = true;
            }
        };
        return FasterBot;
    })(bot.AbstractBot);
    bot.FasterBot = FasterBot;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./RaceBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
var bot;
(function (bot) {
    var Message = comms.Message;

    var FasterBotNic1 = (function (_super) {
        __extends(FasterBotNic1, _super);
        function FasterBotNic1() {
            _super.apply(this, arguments);
            this.throttle = 1;
            this.oldAngle = 0;
            this.lastLaneChoice = "Left";
        }
        FasterBotNic1.prototype.carPositions = function (rec) {
            var positions = rec.data;
            var result = positions.some(function (pos) {
                if (pos.id.name == "797F") {
                    var angle = Math.abs(pos.angle);
                    var angleBefore = this.oldAngle;
                    var nextPiece = GAME.getPiece(pos.piecePosition.pieceIndex + 1);
                    var followingPiece = GAME.getPiece(pos.piecePosition.pieceIndex + 2);
                    if (pos.piecePosition.inPieceDistance > 0.2) {
                        // 0.2     60 75 70 : 8.90
                        // 0.2     60 77 72 : 8.78
                        // 0.2  55 60 79 73 : 8.77 w lane switch
                        // 0.2  52 67 81 72 : 8.77
                        // 0.2  54 68 81 66 : 8.58
                        // 0.2  55 67 81 65 : 8.55 ---------
                        // 0.2  56 68 80 63 : 8.55
                        // 0.2  58 68 75 63 : 8.58
                        // 0.2  58 69 76 65 : 8.55
                        // 0.2  56 69 77 68 : 8.60
                        // 0.2  56 69 77 70 : 8.57
                        // 0.2  58 69 76 66 : 8.52
                        // 0.2 565 69 77 70 : 8.53
                        // 0.2 565 69577 70 : 8.53
                        // 0.2  58 69 76 66 : 8.52
                        if (nextPiece.radius) {
                            if (Math.abs(nextPiece.angle) > 40) {
                                this.throttle = 0.58; // sharp corner, dangerous in 1st corner
                            } else {
                                this.throttle = 0.69; // easy corner, dangerous in 2nd corner
                            }
                        } else {
                            if (angle < angleBefore)
                                this.throttle = 0.76;
                            else
                                this.throttle = 0.665;
                        }

                        /**
                        * Lane switching ...
                        */
                        var nextSwitchId = GAME.findNextSwitchId(pos.piecePosition.pieceIndex);

                        if (GAME.findNextCorner(nextSwitchId).angle > 5) {
                            if (this.lastLaneChoice == "Left") {
                                this.send(new Message('switchLane', "Right"));
                                console.log('\n  SWAPPING TO THE RIGHT');
                                this.lastLaneChoice = "Right";
                            }
                        }
                        if (GAME.findNextCorner(nextSwitchId).angle < -5) {
                            if (this.lastLaneChoice == "Right") {
                                this.send(new Message('switchLane', "Left"));
                                console.log('\n  SWAPPING TO THE LEFT');
                                this.lastLaneChoice = "Left";
                            }
                        }
                        //                        console.log(this.raceBot.findNextCorner(pos.piecePosition.pieceIndex).angle);
                        //                        console.log(this.raceBot.findNextCorner(pos.piecePosition.pieceIndex));
                    }

                    if (GAME.throttle > 1) {
                        GAME.throttle = 1;
                    }
                    if (GAME.throttle < 0.001) {
                        GAME.throttle = 0.001;
                    }
                    this.send(new Message('throttle', GAME.throttle));
                    this.oldAngle = angle;
                    return true;
                }
                return false;
            }, this);
            if (result) {
                rec.handled = true;
                rec.replied = true;
            }
        };
        return FasterBotNic1;
    })(bot.AbstractBot);
    bot.FasterBotNic1 = FasterBotNic1;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./RaceBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
var bot;
(function (bot) {
    var Message = comms.Message;

    var DumbBotNic = (function (_super) {
        __extends(DumbBotNic, _super);
        function DumbBotNic() {
            _super.apply(this, arguments);
            this.throttle = 1;
            this.oldThrottle = 1;
            this.oldAngle = 0;
            this.lastLaneChoice = "Left";
        }
        DumbBotNic.prototype.gameInit = function (msg) {
            this.pieces = msg.data.race.track.pieces;

            this.pieces.forEach(function (piece, i) {
                piece['id'] = i;

                if (piece.angle > 5)
                    piece['turn'] = '//////';
                else if (piece.angle < 5)
                    piece['turn'] = '\\\\\\';

                console.log(piece);
            });
        };

        DumbBotNic.prototype.carPositions = function (rec) {
            var positions = rec.data;
            var result = positions.some(function (pos) {
                if (pos.id.name == "797F") {
                    var angle = Math.abs(pos.angle);
                    var angleBefore = this.oldAngle;
                    var piece = GAME.thisPiece;
                    var nextPiece = GAME.getPiece(pos.piecePosition.pieceIndex + 1);
                    var followingPiece = GAME.getPiece(pos.piecePosition.pieceIndex + 2);
                    var idx = pos.piecePosition.pieceIndex;

                    var t = 0.6;
                    var speed = {
                        0: 0.8,
                        1: 0.6,
                        2: 0.5,
                        3: 0.2,
                        4: 0.3,
                        5: 0.3,
                        6: 0.4,
                        14: 0.2,
                        15: 0.3
                    };

                    if (speed[idx])
                        t = speed[idx];
                    if (piece.angle)
                        t = 0.5;
                    else
                        t = 0.75;

                    GAME.throttle = t;

                    /**
                    * Lane switching ...
                    */
                    var nextSwitchId = GAME.findNextSwitchId(pos.piecePosition.pieceIndex);

                    if (GAME.findNextCorner(nextSwitchId).angle > 5) {
                        if (this.lastLaneChoice == "Left") {
                            this.send(new Message('switchLane', "Right"));
                            console.log('\n  SWAPPING TO THE RIGHT');
                            this.lastLaneChoice = "Right";
                        }
                    }
                    if (GAME.findNextCorner(nextSwitchId).angle < -5) {
                        if (this.lastLaneChoice == "Right") {
                            this.send(new Message('switchLane', "Left"));
                            console.log('\n  SWAPPING TO THE LEFT');
                            this.lastLaneChoice = "Left";
                        }
                    }

                    if (GAME.throttle > 1) {
                        GAME.throttle = 1;
                    }
                    if (GAME.throttle < 0.001) {
                        GAME.throttle = 0.001;
                    }

                    this.send(new Message('throttle', GAME.throttle));

                    if (GAME.throttle != this.oldThrottle) {
                        this.oldThrottle = GAME.throttle;
                        console.log("\nnew throttle " + GAME.throttle);
                    }
                    this.oldAngle = angle;
                    return true;
                }
                return false;
            }, this);
            if (result) {
                rec.handled = true;
                rec.replied = true;
            }
        };
        return DumbBotNic;
    })(bot.AbstractBot);
    bot.DumbBotNic = DumbBotNic;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
/// <reference path="./track"/>
var bot;
(function (bot) {
    var Message = comms.Message;

    var SwitchBot = (function (_super) {
        __extends(SwitchBot, _super);
        function SwitchBot() {
            _super.apply(this, arguments);
            this.lastIndex = -2;
        }
        SwitchBot.prototype.carPositions = function (rec) {
            if (GAME.crashed)
                return;
            if (GAME.nextCorner === null)
                return;
            if (GAME.thisPosition.piecePosition.pieceIndex != this.lastIndex) {
                var nextPiece = GAME.getPiece(GAME.thisPosition.piecePosition.pieceIndex + 1);
                var toNextPiece = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;
                if (nextPiece.switch && toNextPiece < 30) {
                    var angle = 0;
                    var index = GAME.thisPosition.piecePosition.pieceIndex + 2;
                    while (true) {
                        var piece = GAME.getPiece(index);
                        if (piece.switch) {
                            break;
                        }
                        if (piece.angle) {
                            angle += piece.angle;
                        }
                        index++;
                    }
                    var direction;
                    if (angle < -5)
                        direction = "Left";
                    else if (angle > 5)
                        direction = "Right";
                    if (!direction)
                        return;
                    GAME.state = "Next piece is a switch piece, switching " + direction;
                    this.send(new Message('switchLane', direction));
                    rec.handled = true;
                    rec.replied = true;
                    this.lastIndex = GAME.thisPosition.piecePosition.pieceIndex;
                }
            }
        };
        return SwitchBot;
    })(bot.AbstractBot);
    bot.SwitchBot = SwitchBot;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
/// <reference path="./track"/>
/// <reference path="./game"/>

var bot;
(function (bot) {
    var Message = comms.Message;

    var GlenBot = (function (_super) {
        __extends(GlenBot, _super);
        function GlenBot() {
            _super.apply(this, arguments);
            // Make this bigger to brake later into corners
            this.brakeForce = 0.15;
            // Make this bigger to make all corners faster
            this.brakeOffset = 2;
            // Make this bigger to make corners with larger radius faster
            this.brakeEffect = 13.3;
            // Change how the radius affects braking speed/distance
            this.radiusModifier = 1.393582722;
            // make this lower to brake faster when sliding out
            this.reactiveBrake = 20.16;
            this.slidingBrake = 0.5;
            // Make this bigger to accelerate through corners faster
            this.reactiveThrottle = 0.1;
            // Make this bigger to coast through corners faster
            this.coastThrottle = 0.01;
        }
        GlenBot.prototype.gameInit = function (msg) {
            console.log("Corner calc");
            console.log("  ", 50, this.getCornerSpeed(50, 5), "should be about 4");
            console.log("  ", 100, this.getCornerSpeed(100, 5), "should be about 6");
            console.log("  ", 200, this.getCornerSpeed(200, 5), "should be about 10");
        };

        GlenBot.prototype.getCornerSpeed = function (radius, mass) {
            var speedDivisor = mass * (this.brakeEffect * (GAME.car.dimensions.guideFlagPosition / (GAME.car.dimensions.length / 2)));
            return ((this.radiusModifier * radius) / speedDivisor) + this.brakeOffset;
        };

        GlenBot.prototype.carPositions = function (rec) {
            if (rec.handled)
                return;

            if (GAME.crashed) {
                rec.handled = true;
                return;
            }

            var absAngle = Math.abs(GAME.thisPosition.angle);

            //            var lastLastPiece = GAME.getPiece(GAME.thisPosition.piecePosition.pieceIndex - 2);
            var nextPiece = GAME.getPiece(GAME.thisPosition.piecePosition.pieceIndex + 1);
            var toNextPiece = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;

            GAME.targetCornerSpeed = 100;
            if (GAME.nextCorner && GAME.carMass) {
                GAME.targetCornerSpeed = this.getCornerSpeed(GAME.nextCornerRadius, GAME.carMass);
                //                console.log("Radius: " + GAME.nextCorner.radius.toFixed(0) + " Target: ", targetCornerSpeed.toFixed(2));
            }

            var brakingDistance = 0;
            if (GAME.speed > GAME.targetCornerSpeed) {
                for (var i = GAME.speed; i > GAME.targetCornerSpeed; i -= this.brakeForce) {
                    brakingDistance += i;
                }
            }

            //            console.log("Braking distance:", brakingDistance, ",", GAME.distanceToCorner);
            if (GAME.canSeeFinish) {
                GAME.state = "Sprint to finish line";
                GAME.throttle = 1;
            } else if (brakingDistance > GAME.distanceToCorner && GAME.speed > GAME.targetCornerSpeed) {
                GAME.state = "Preemptive braking";
                GAME.throttle = 0.00;
            } else if (nextPiece.length && toNextPiece < 50 && absAngle < this.lastAngle) {
                GAME.state = "Throttle up into straight";
                GAME.throttle = 1;
            } else if (GAME.thisPiece.length) {
                GAME.state = "Straight";
                GAME.throttle = 1;
            } else {
                //                if ((GAME.thisPosition.angle < -1 && GAME.thisPiece.angle > 0) ||
                //                    (GAME.thisPosition.angle > 1 && GAME.thisPiece.angle < 0)) {
                //                    GAME.state = "Brake about to swing out";
                //                    GAME.throttle = 0;
                //                } else
                // Slow down propotionately to the angle we are on
                if (absAngle > this.lastAngle && absAngle - this.lastAngle > 1) {
                    GAME.state = "Brake because we are sliding out";
                    GAME.throttle -= absAngle / this.reactiveBrake;
                    GAME.throttle = Math.max(GAME.throttle, this.slidingBrake);
                } else {
                    //                    if (absAngle < this.lastAngle) {
                    //                        GAME.state = "Increase drift";
                    //                        GAME.throttle += this.reactiveThrottle;
                    //                    } else {
                    //                        GAME.state = "Coast through corner";
                    //                        // Do nothing?
                    //                        GAME.throttle += this.coastThrottle;
                    //                    }
                    //                    GAME.throttle += 3/absAngle; // speed up faster depending on angle
                    //                    GAME.throttle = GAME.speed / 10;
                    //                    if (this.lastSpeed < GAME.speed)
                    //                        GAME.throttle += 0.1;
                    //                    else if (this.lastSpeed - GAME.speed > 0.2) {
                    //                        GAME.throttle -= 0.1;
                    //                    }
                    GAME.state = "Accelerate through corner";
                    GAME.throttle = 1;
                }
            }

            this.lastSpeed = GAME.speed;

            if (GAME.throttle > 1) {
                GAME.throttle = 1;
            }
            if (GAME.throttle < 0) {
                GAME.throttle = 0;
            }
            if (isNaN(GAME.throttle)) {
                GAME.throttle = 0;
            }
            if (GAME.speed == 0 && GAME.throttle == 0) {
                GAME.state = "Stalled :)";
                GAME.throttle = 1;
            }
            this.lastAngle = absAngle;

            //            console.log("Sending throttle " + GAME.throttle);
            this.send(new Message('throttle', GAME.throttle));
            rec.handled = true;
            rec.replied = true;
        };

        GlenBot.prototype.crash = function (rec) {
            trace(JSON.stringify(rec, null, 2));

            // we can't do this it will quit even on qualifying
            if (TRIAL && rec.data.name == "797F")
                setTimeout(function () {
                    process.exit();
                }, 10000);
        };
        return GlenBot;
    })(bot.AbstractBot);
    bot.GlenBot = GlenBot;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./race"/>
/// <reference path="./track"/>
var bot;
(function (bot) {
    var fs = require("fs");

    var LoggingBot = (function (_super) {
        __extends(LoggingBot, _super);
        function LoggingBot() {
            _super.apply(this, arguments);
        }
        //LOG:any;
        LoggingBot.prototype.join = function (rec) {
            console.log('Joined');
        };

        LoggingBot.prototype.yourCar = function (msg) {
            //this.LOG = fs.createWriteStream("./log" + GAME.carColor + ".csv", {flags: 'w', encoding: 'UTF-8'});
        };

        LoggingBot.prototype.tournamentEnd = function (rec) {
            // this.LOG.close();
        };

        LoggingBot.prototype.crash = function (msg) {
            console.log("Crash:", msg.data);
            if (msg.data.color != GAME.carColor)
                return;
            console.log(GAME.state);
        };

        LoggingBot.prototype.spawn = function (msg) {
            if (msg.data.color != GAME.carColor)
                return;
            console.log(GAME.state);
        };

        LoggingBot.prototype.gameInit = function (msg) {
            //            console.log("Track: ", GAME.race.track.pieces);
        };

        LoggingBot.prototype.carPositions = function (rec) {
            if (!GAME.started || GAME.crashed)
                return;

            var toNextPiece = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;
            this.log([
                GAME.thisPosition.piecePosition.lap,
                GAME.thisPosition.piecePosition.pieceIndex,
                GAME.throttle.toFixed(0),
                GAME.thisPosition.angle.toFixed(1),
                GAME.speed.toFixed(2),
                GAME.targetCornerSpeed.toFixed(2),
                GAME.thisPiece.radius ? GAME.thisPiece.radius.toFixed(0) : '',
                GAME.thisPiece.angle ? GAME.thisPiece.angle.toFixed(1) : '',
                GAME.thisPiece.angle ? Math.abs(GAME.thisPiece.angle).toFixed(1) : '',
                GAME.distanceToCorner.toFixed(1),
                GAME.state,
                GAME.turboOn ? "Turbo" : (GAME.turbo ? "T" : ""),
                GAME.fastestLap == -1 ? "" : GAME.fastestLap.toFixed(0)
            ].join(", "));

            console.log([
                GAME.thisPosition.piecePosition.lap,
                GAME.thisPosition.piecePosition.pieceIndex,
                (GAME.throttle * 100).toFixed(0) + '%',
                GAME.thisPosition.angle.toFixed(1) + '°',
                GAME.speed.toFixed(2),
                GAME.targetCornerSpeed.toFixed(1),
                GAME.nextCornerRadius.toFixed(0),
                GAME.thisPiece.angle ? (GAME.thisPiece.radius.toFixed(0) + " " + GAME.thisPiece.angle.toFixed(0) + '° ' + ((GAME.thisPiece.angle < 0) ? "Left Turn" : "Right Turn")) : "Straight",
                GAME.distanceToCorner.toFixed(1),
                GAME.state,
                GAME.turboOn ? "Turbo" : (GAME.turbo ? "T" : ""),
                GAME.fastestLap == -1 ? "" : GAME.fastestLap.toFixed(0)
            ].join(", "));
        };

        LoggingBot.prototype.gameStart = function (rec) {
            console.log('Race started');
            console.log('Log format');
            var format = 'Lap,Piece,Throttle,CarAngle,Speed,TargetSpeed,PieceRadius,PieceAngle,PieceAbsAngle,PieceType,ToNextCorner,ThrottleState,Turbo,FastestLap';
            this.log(format);
            var format = 'Lap,Piece,Throttle,Angle,Speed,TargetSpeed,CornerRadius,PieceType,ToNextCorner,ThrottleState,Turbo,FastestLap';
            console.log(format);
            GAME.started = true;
            rec.handled = true;
        };

        LoggingBot.prototype.gameEnd = function (msg) {
            console.log('Race ended');
            console.log('Results\n', msg.data.results);
            console.log('Best Laps\n', msg.data.bestLaps);
        };

        LoggingBot.prototype.log = function (msg) {
            //            console.log(msg);
            //            this.LOG.write(msg);
            //            this.LOG.write("\n");
        };
        return LoggingBot;
    })(bot.AbstractBot);
    bot.LoggingBot = LoggingBot;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
var bot;
(function (bot) {
    var UnhandledBot = (function (_super) {
        __extends(UnhandledBot, _super);
        function UnhandledBot() {
            _super.apply(this, arguments);
        }
        UnhandledBot.prototype.handleMessage = function (msg) {
            // reply to the server
            if (!msg.handled)
                console.log('\n--Message: ' + msg.msgType + ' : ', msg.data);
            msg.handled = true;
            if (!msg.replied)
                this.ping();
            msg.replied = true;
        };
        return UnhandledBot;
    })(bot.AbstractBot);
    bot.UnhandledBot = UnhandledBot;
})(bot || (bot = {}));
/// <reference path="./AbstractBot"/>
/// <reference path="./comms"/>
/// <reference path="./game"/>

var bot;
(function (bot) {
    var Message = comms.Message;

    var TurboBot = (function (_super) {
        __extends(TurboBot, _super);
        function TurboBot() {
            _super.apply(this, arguments);
            this.startTurboBeforeStraight = 30;
            this.requiredTurboDistance = 400;
        }
        TurboBot.prototype.gameInit = function (msg) {
            console.log("TurboBot overriding requiredTurboDistance based on track");
            this.requiredTurboDistance = GAME.longestStraightPiece - 20;
            console.log("  Setting to:" + this.requiredTurboDistance);
        };

        TurboBot.prototype.carPositions = function (msg) {
            if (msg.handled || !GAME.turbo || GAME.crashed)
                return;

            if (GAME.longestStraightPieceIndexes.indexOf(GAME.thisPosition.piecePosition.pieceIndex + 1) == -1)
                return;

            var toNextPiece = GAME.thisLength - GAME.thisPosition.piecePosition.inPieceDistance;
            if (GAME.thisPiece.angle && toNextPiece > this.startTurboBeforeStraight)
                return;

            GAME.state = "TURBO!";
            console.log("TURBOOOOOOOOOOO:", GAME.distanceToCorner);
            this.send(new Message('turbo', 'Kiss your butt good bye'));
            GAME.turbo = null;
            msg.handled = true;
            msg.replied = true;
        };
        return TurboBot;
    })(bot.AbstractBot);
    bot.TurboBot = TurboBot;
})(bot || (bot = {}));
/// <reference path="./comms" />
/// <reference path="./race" />
/// <reference path="./game" />
/// <reference path="./SimpleBot.ts" />
/// <reference path="./NicBot1" />
/// <reference path="./NicBot2" />
/// <reference path="./SimpleBot" />
/// <reference path="./FasterBot" />
/// <reference path="./FasterBotNic1.ts" />
/// <reference path="./DumbBotNic.ts" />
/// <reference path="./RaceBot" />
/// <reference path="./SwitchBot" />
/// <reference path="./GlenBot" />
/// <reference path="./LoggingBot" />
/// <reference path="./UnhandledBot" />
/// <reference path="./TurboBot" />

var Comms = comms.Comms;

var serverHost = "testserver.helloworldopen.com";
var GAME = new game.GameState();
var TRIAL = true;

//var serverHost:string = "webber.helloworldopen.com";
var serverPort = "8091";
var botName = "797F";
var botKey = "i237nvaJ2cPdOQ";
var tr = null;
var numCars = 1;
console.log("Args: " + process.argv.length);
if (process.argv.length < 7) {
    console.log("Usage: trial <host> <port> <botName> <botKey> <trackName> <numberCars>\n" + "  <turboBeforeCorner>\n" + "  <slidingBrake> <reactiveBrake>");
    console.log("------");
    console.log("turboBeforeCorner (35) = max distance before straight turbo will fire, may fire later depending on server ticks/speed");
    console.log("------");
    console.log("slidingBrake (0.5) = minimum throttle when sliding out");
    console.log("reactiveBrake (20) = Make this lower to brake faster when sliding out \n" + "  formula is throttle -= absAngle / reactiveBrake");

    process.exit(1);
}
if (process.argv.length > 1)
    serverHost = process.argv[2];
if (process.argv.length > 2)
    serverPort = process.argv[3];
if (process.argv.length > 3)
    botName = process.argv[4];
if (process.argv.length > 4)
    botKey = process.argv[5];
if (process.argv.length > 5)
    tr = process.argv[6];
if (process.argv.length > 6)
    numCars = process.argv[7];

var ai = new bot.GlenBot();

var switchBot = new bot.SwitchBot();
var raceBot = new bot.RaceBot();
var log = new bot.LoggingBot();
var unhandled = new bot.UnhandledBot();
var turboBot = new bot.TurboBot();

if (process.argv.length > 7)
    turboBot.startTurboBeforeStraight *= parseFloat(process.argv[8]);

if (process.argv.length > 8)
    ai.slidingBrake *= parseFloat(process.argv[9]);
if (process.argv.length > 9)
    ai.reactiveBrake *= parseFloat(process.argv[10]);

console.log("Final params");
console.log([
    turboBot.startTurboBeforeStraight,
    ai.slidingBrake,
    ai.reactiveBrake
].join(" "));

// switchBot, turboBot,
var server = new Comms(serverHost, serverPort, botName, botKey, [raceBot, switchBot, turboBot, ai, log, unhandled]);

server.connect(function () {
    console.log("Connected");
    if (tr) {
        return server.joinRace(tr, numCars);
    } else {
        return server.join();
    }
});

// keimola
// germany
// usa
function trace(anything) {
    console.log(anything);
}
