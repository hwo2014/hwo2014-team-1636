/// <reference path="./comms" />
/// <reference path="./race" />
/// <reference path="./game" />
/// <reference path="./SimpleBot.ts" />
/// <reference path="./NicBot1" />
/// <reference path="./NicBot2" />
/// <reference path="./SimpleBot" />
/// <reference path="./FasterBot" />
/// <reference path="./FasterBotNic1.ts" />
/// <reference path="./DumbBotNic.ts" />
/// <reference path="./RaceBot" />
/// <reference path="./SwitchBot" />
/// <reference path="./GlenBot" />
/// <reference path="./LoggingBot" />
/// <reference path="./UnhandledBot" />
/// <reference path="./TurboBot" />


declare var process:any;

import Comms = comms.Comms;
import IMessageHandler = comms.IMessageHandler;

var serverHost:string = "testserver.helloworldopen.com";
var GAME:game.GameState = new game.GameState();
var TRIAL:boolean = true;
//var serverHost:string = "webber.helloworldopen.com";
var serverPort:string = "8091";
var botName:string = "797F";
var botKey:string = "i237nvaJ2cPdOQ";
var tr:string = null;
var numCars:number = 1;
console.log("Args: " + process.argv.length);
if (process.argv.length < 7) {
    console.log("Usage: trial <host> <port> <botName> <botKey> <trackName> <numberCars>\n" +
        "  <turboBeforeCorner>\n" +
        "  <slidingBrake> <reactiveBrake>");
    console.log("------");
    console.log("turboBeforeCorner (35) = max distance before straight turbo will fire, may fire later depending on server ticks/speed");
    console.log("------");
    console.log("slidingBrake (0.5) = minimum throttle when sliding out");
    console.log("reactiveBrake (20) = Make this lower to brake faster when sliding out \n" +
        "  formula is throttle -= absAngle / reactiveBrake");

    process.exit(1);
}
if (process.argv.length > 1)
    serverHost = process.argv[2];
if (process.argv.length > 2)
    serverPort = process.argv[3];
if (process.argv.length > 3)
    botName = process.argv[4];
if (process.argv.length > 4)
    botKey = process.argv[5];
if (process.argv.length > 5)
    tr = process.argv[6];
if (process.argv.length > 6)
    numCars = process.argv[7];

var ai:bot.GlenBot = new bot.GlenBot();

var switchBot:bot.SwitchBot = new bot.SwitchBot();
var raceBot:bot.RaceBot = new bot.RaceBot();
var log:bot.LoggingBot = new bot.LoggingBot();
var unhandled:bot.UnhandledBot = new bot.UnhandledBot();
var turboBot:bot.TurboBot = new bot.TurboBot();


if (process.argv.length > 7)
    turboBot.startTurboBeforeStraight *= parseFloat(process.argv[8]);

if (process.argv.length > 8)
    ai.slidingBrake *= parseFloat(process.argv[9]);
if (process.argv.length > 9)
    ai.reactiveBrake *= parseFloat(process.argv[10]);

console.log("Final params");
console.log([
    turboBot.startTurboBeforeStraight,
    ai.slidingBrake,
    ai.reactiveBrake
].join(" "));
// switchBot, turboBot,
var server:Comms = new Comms(
    serverHost, serverPort, botName, botKey,
    [raceBot, switchBot, turboBot, ai, log, unhandled]);

server.connect(function ():boolean {
    console.log("Connected");
    if (tr) {
        return server.joinRace(tr,numCars);
    } else {
        return server.join();
    }
});

// keimola
// germany
// usa


function trace(anything) {
    console.log(anything);
}
