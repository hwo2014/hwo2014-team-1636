# Options
Need to determine the following:

 * Can you spin out by accelerating too fast? i.e. from 0.1 to 1.0 coming out of a corner
 * What do collisions do
 * what do collisions on a switch do
 * what penalty for using switch
 * Find accelerate out point for corner pieces maybe custom per piece
 * Find brake point per piece including switch if switching

Find folowing:

 * optimum path around track
 * length of all tracks without switch
 * rate cornering ability of car config

## Most basic

* Keep increasing throttle until we crash then reduce every time we crash.
* Finds optimum constant speed around the track.  May take a long time

## Best corner speed

* Throttle at 1.0 until we crash
* Throttle down until out of corner - hope to find optimum speed per corner
* Record best speed for each corner
* on second lap set best speed for that corner

## Reactive

* Throttle at 1.0
* if Angle != 0 reduce throttle
* record throttle for each piece

## Reactive shortest path

* switch to travel smallest distance

## Reactive don't follow opponent

* switch away from lanes if approaching opponent

## Rate corners

* rate corner pieces by angle and radius (per lane)
* as we travel over a lane record speed and assign that min speed to faster lane pieces
* if we crash we can rate max speed on slower lane pieces

# Likely best

* Combination of Rate Corners, and Reactive and dont follow opponent
