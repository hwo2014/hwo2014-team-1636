@echo off

setlocal

for /f "tokens=2,3 delims== " %%i in (config) do echo set %%i=%%j
call scripts\check_main_args %*

if ERRORLEVEL 1 goto end

if "" == %3 set BOTNAME=%3

cd %PL% && call run %1 %2 %BOTNAME% %BOTKEY%

:end
endlocal