@echo off

if "" == "%2" goto usage
goto end

:usage
  echo "usage: %~nx0 <host> <port> [<botname>]"
  exit /b 1

:end