@echo off

if "" == "%4" goto usage

goto end

:usage
  echo "usage: %~nx0 <host> <port> <botname> <botkey>"
  exit /b 1

:end

